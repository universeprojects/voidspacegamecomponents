package com.universeprojects.gamecomponents.client.demos;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ERectangle;

public class ScrollablePaneDemo extends Demo {

    private final H5ELayer layer;
    private final GCSimpleWindow window;

    public ScrollablePaneDemo(H5ELayer layer) {
        this.layer = Dev.checkNotNull(layer);

        window = new GCSimpleWindow(layer, "scrollable-pane-demo", "Scrollable Pane Demo", 500, 500, false);
        window.positionProportionally(.5f, .5f);

        H5EScrollablePane pane1 = new H5EScrollablePane(layer);
        window.row();
        final Cell<H5EScrollablePane> pane1Cell = window.add(pane1);
        pane1Cell.prefSize(225, 300);
        pane1Cell.fillX();
        addItems(pane1, 8, 80, 10, 1.6f, "Widget");

        H5EScrollablePane pane2 = new H5EScrollablePane(layer);
        final Cell<H5EScrollablePane> pane2Cell = window.add(pane2);
        pane2Cell.prefSize(225, 300);
        addItems(pane2, 10, 40, 5, 1f, "Small wooden club -");

        H5EScrollablePane pane3 = new H5EScrollablePane(layer);
        window.addEmptyLine(15);
        final Cell<H5EScrollablePane> pane3Cell = window.add(pane3);
        pane3Cell.prefSize(475, 120);
        addItems(pane3, 12, 10, 5, 0.6f, "The quick brown fox jumps over the lazy dog. Sphinx of black quartz -");

    }

    private void addItems(H5EScrollablePane pane, int itemCount, int itemHeight, int margin, float fontScale, String text) {
        int maxY = 0;
        for (int i = 0; i < itemCount; i++) {
            H5ERectangle rect = new H5ERectangle(layer);
            pane.getContent().row();
            final Cell<H5ERectangle> cell = pane.getContent().add(rect);
            cell.padTop(margin);
            rect.setColor(Color.valueOf("#999999"));
            cell.prefHeight(itemHeight);

            H5ELabel label = new H5ELabel(layer);
            pane.getContent().add(label);
            label.setText(text + " " + (i + 1));
            label.setColor(Color.valueOf("#FFFFFF"));
            label.setFontScale(fontScale);
            label.setOrigin(Align.center);
//            label.setProportionX(.5);
//            label.setProportionY(.5);

            maxY = (int) UPMath.max(maxY, rect.getY() + rect.getHeight());
        }

//        pane.setContentHeight(maxY);
    }

    @Override
    public void open() {
        window.open();
    }

    @Override
    public void close() {
        window.close();
    }

    @Override
    public boolean isOpen() {
        return window.isOpen();
    }


}
