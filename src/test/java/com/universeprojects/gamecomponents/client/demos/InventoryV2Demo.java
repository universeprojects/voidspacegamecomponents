package com.universeprojects.gamecomponents.client.demos;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.RandomUtils;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventory;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryBase;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.InventoryAction;
import com.universeprojects.gamecomponents.client.dialogs.inventory.SlotConfig;
import com.universeprojects.gamecomponents.client.dialogs.inventory.TabbedInventoryComponent;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class InventoryV2Demo extends Demo {

    private final H5ELayer layer;

    private final GCSimpleWindow controlWindow;

    private final GCSimpleWindow inventoryWindow;

    //private final H5EButton btnSuitInventory;
    //private final H5EButton btnShipInventory1;
    //private final H5EButton btnShipInventory2;
    //private final ButtonGroup<H5EButton> buttons;

    /////////////////////
    private final TabbedInventoryComponent tabComp;

    public InventoryV2Demo(H5ELayer layer) {
        this.layer = Dev.checkNotNull(layer);

        controlWindow = new GCSimpleWindow(layer, "inventory-viewer-demo", "Inventory Viewer Demo");
        controlWindow.onClose.registerHandler(this::cleanup);
        controlWindow.row().padTop(40).padBottom(40);
        controlWindow.positionProportionally(.6f, .15f);

        inventoryWindow = new GCSimpleWindow(layer, "inventory", "Cargo Hold", 670, 390);
        inventoryWindow.setPackOnOpen(false);
        inventoryWindow.positionProportionally(0.6f, 0.6f);

        ////////////
        tabComp = new TabbedInventoryComponent(layer);

//        inventoryWindow.debugAll();
/*
        btnSuitInventory = ButtonBuilder
                        .inLayer(layer)
                        .withStyle("button2-tab")
                        .withForegroundSpriteType("btn-character-equipment")
                        .build();
        btnShipInventory1 = ButtonBuilder
                        .inLayer(layer)
                        .withStyle("button2-tab")
                        .withForegroundSpriteType("btn-ship-equipment")
                        .build();
/*        btnShipInventory2 = ButtonBuilder
                        .inLayer(layer)
                        .withStyle("button2-tab")
                        .withForegroundSpriteType("tab-knowledge")
                        .build();
*/
        tabComp.add("tab-knowledge", new GCInventoryDemoImpl(layer));
        tabComp.add("tab-knowledge", new GCInventoryDemoImpl(layer));
        inventoryWindow.add(tabComp).grow().top();
        tabComp.add("tab-knowledge", new GCInventoryDemoImpl(layer));
/*
        final HorizontalGroup horizontalGroup = inventoryWindow.add(new HorizontalGroup()).
                padTop(10).padBottom(10).left().getActor();
        horizontalGroup.space(0).padLeft(10);
*/
        //final H5EButton[] buttonArray = {btnSuitInventory, btnShipInventory1/*, btnShipInventory2*/};

        final int TAB_BUTTON_WIDTH  = 30;
        final int TAB_BUTTON_HEIGHT = 25;
/*
        for (H5EButton btn : buttonArray) {
            horizontalGroup.addActor(btn);
            btn.getImageCell()
                    .width(TAB_BUTTON_WIDTH)
                    .height(TAB_BUTTON_HEIGHT)
                    .align(Align.center);
        }
        buttons = new ButtonGroup<>(buttonArray);
        buttons.setMaxCheckCount(1);
        buttons.setMinCheckCount(1);
*/
        //inventoryWindow.row();

/*
        final GCInventory inventory = new GCInventoryDemoImpl(layer);
        inventoryWindow.add(inventory).grow();
        inventoryWindow.onClose.registerHandler(inventory::clearComponents);
*/

        //btnSuitInventory.addCheckedButtonListener(this::loadSuitInventory);


        H5EButton btnEnableTransfer = addControlButton(0, "Enable transfer");
        btnEnableTransfer.addButtonListener(() -> {
//            generalViewer.setTransferEnabled(true);
//            singleStackViewer.setTransferEnabled(true);
        });
        H5EButton btnDisableTransfer = addControlButton(10, "Disable transfer");
        btnDisableTransfer.addButtonListener(() -> {
//            generalViewer.setTransferEnabled(false);
//            singleStackViewer.setTransferEnabled(false);
        });
    }

    private H5EButton addControlButton(int offsetX, String caption) {
        H5EButton btn = new H5EButton(caption, layer);
        return controlWindow.add(btn).padLeft(offsetX).fill().getActor();
    }

    @Override
    public void open() {
        controlWindow.open();
        inventoryWindow.open();
//        singleStackViewer.open();
//        generalViewer.open();
    }

    @Override
    public void close() {
        controlWindow.close();
    }

    private void cleanup() {
        inventoryWindow.close();
    }

    @Override
    public boolean isOpen() {
        return inventoryWindow.isOpen();
    }

    static class GCInventoryDemoItem implements GCInventoryItem {

        private final String name;
        private final String itemClass;
        private final long quantity;
        private final String iconSpriteTypeKey;
        private final String rarityIconStyle;
        private final boolean stackable;

        public GCInventoryDemoItem(String name, String iconSpriteTypeKey, long quantity) {
            this(name, name, quantity, iconSpriteTypeKey);
        }

        public GCInventoryDemoItem(String name, String itemClass, long quantity, String iconSpriteTypeKey) {
            this(name, itemClass, quantity, iconSpriteTypeKey, randomRarityIconStyle(), quantity > 1 || RandomUtils.getRandom().nextBoolean());
        }

        private static String randomRarityIconStyle() {
            String[] styles = new String[]{"junk_grade", "common_grade",
                "uncommon_grade", "rare_grade", "epic_grade", "legendary_grade"};
            final int index = RandomUtils.random(styles.length - 1);
            return "icon-"+ styles[index];
        }

        public GCInventoryDemoItem(String name, String itemClass, long quantity, String iconSpriteTypeKey, String rarityIconStyle, boolean stackable) {
            this.name = name;
            this.itemClass = itemClass;
            this.quantity = quantity;
            this.iconSpriteTypeKey = iconSpriteTypeKey;
            this.rarityIconStyle = rarityIconStyle;
            this.stackable = stackable;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getItemClass() {
            return itemClass;
        }

        @Override
        public String getDescription() {
            return "";
        }

        @Override
        public long getQuantity() {
            return quantity;
        }

        @Override
        public boolean isStackable() {
            return stackable;
        }

        @Override
        public String getIconSpriteKey() {
            return iconSpriteTypeKey;
        }

        public String getRarityIconStyle() {
            return rarityIconStyle;
        }

        @Override
        public Map<String, String> getAdditionalProperties() {
            return Collections.emptyMap();
        }

        @Override
        public void createAdditionalInspectorComponents(Table inspectorTable) {

        }
    }

    static class GCInventoryDemoImpl extends GCInventory<GCInventoryDemoItem> {

        private Logger log = Logger.getLogger(GCInventoryDemoImpl.class);

        public GCInventoryDemoImpl(H5ELayer layer, int count) {
            super(layer, count);

            addItem(1,  new InventoryV2Demo.GCInventoryDemoItem("Metal", "metal-icon", 20));
            addItem(2,  new InventoryV2Demo.GCInventoryDemoItem("Metal", "metal-icon", 13));
            addItem(3,  new InventoryV2Demo.GCInventoryDemoItem("Hydrogen", "hydrogen-icon", 1553));
            addItem(4,  new InventoryV2Demo.GCInventoryDemoItem("Hydrogen", "hydrogen-icon", 738));
            addItem(5,  new InventoryV2Demo.GCInventoryDemoItem("Standard Missile", "missile1-icon", 6));
            addItem(6,  new InventoryV2Demo.GCInventoryDemoItem("Plasma Blaster", "turret1-icon", 1));
        }

        @Override
        protected SlotConfig getSlotConfig(Integer key) {
            return new SlotConfig() {
                @Override
                public String getName() {
                    return null;
                }

                @Override
                public String getEmptySlotIcon() {
                    return null;
                }

                @Override
                public String getDescription() {
                    return null;
                }
            };
        }

        public GCInventoryDemoImpl(H5ELayer layer) {
            super(layer, 30);

            addItem(1,  new GCInventoryDemoItem("Metal", "metal-icon", 20));
            addItem(2,  new GCInventoryDemoItem("Metal", "metal-icon", 13));
            addItem(3,  new GCInventoryDemoItem("Hydrogen", "hydrogen-icon", 1553));
            addItem(4,  new GCInventoryDemoItem("Hydrogen", "hydrogen-icon", 738));
            addItem(5,  new GCInventoryDemoItem("Standard Missile", "missile1-icon", 6));
            addItem(6,  new GCInventoryDemoItem("Plasma Blaster", "turret1-icon", 1));
            addItem(12,  new GCInventoryDemoItem("Escape Pod", "drone1-icon", 1));
            addItem(13,  new GCInventoryDemoItem("Space Garbage", "drone1-icon", 3));
            addItem(17,  new GCInventoryDemoItem("Space Garbage", "drone1-icon", 11));
            addItem(18, new GCInventoryDemoItem("Space Garbage", "drone1-icon", 27));
        }

        @Override
        protected List<InventoryAction> getActions(Integer key) {
            return Collections.emptyList();
        }

        @Override
        protected <KT> void onCrossInventoryDropFromHere(Integer startKey, GCInventoryDemoItem item, GCInventoryBase<KT, ?, ?> targetInventory, KT targetKey) {
            log.info("Cross-Inventory drop from here: "+startKey+", "+item+", "+targetInventory,", "+targetKey);
        }

        @Override
        protected <KS, I extends GCInventoryItem> void onCrossInventoryDropToHere(GCInventoryBase<KS, ?, ?> startInventory, KS startKey, I item, Integer targetKey) {
            log.info("Cross-Inventory drop to here: "+startInventory+", "+startKey+", "+item+", "+targetKey);
        }

        @Override
        protected void onSameInventoryDrop(Integer startKey, GCInventoryDemoItem item, Integer targetKey) {
            log.info("Same-Inventory drop: "+startKey+", "+item+", "+targetKey);
        }

        @Override
        protected <KT> boolean canCrossInventoryDropFromHere(Integer startKey, GCInventoryDemoItem item, GCInventoryBase<KT, ?, ?> targetInventory, KT targetKey) {
            return true;
        }

        @Override
        protected <KS, I extends GCInventoryItem> boolean canCrossInventoryDropToHere(GCInventoryBase<KS, ?, ?> startInventory, KS startKey, I item, Integer targetKey) {
            return true;
        }

        @Override
        protected boolean canSameInventoryDrop(Integer startKey, GCInventoryDemoItem item, Integer targetKey) {
            return true;
        }

        @Override
        protected void jettison(Integer startKey, GCInventoryDemoItem item) {

        }

        @Override
        protected boolean canJettison(GCInventoryDemoItem item) {
            return false;
        }

        @Override
        protected void onMergeAll(GCInventoryDemoItem item) {

        }

        @Override
        protected void onStackSplit(Integer key, GCInventoryDemoItem item, int amountToSplit) {

        }

        @Override
        public void update () {

        }
    }

}
