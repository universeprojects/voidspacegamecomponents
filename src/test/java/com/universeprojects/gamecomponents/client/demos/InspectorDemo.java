package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.gamecomponents.client.dialogs.GCObjectInspector;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.LinkedHashMap;
import java.util.Map;

public class InspectorDemo extends Demo {

    private final GCObjectInspector inspector;

    public InspectorDemo(H5ELayer layer) {

        Map<String, String> props = new LinkedHashMap<>();
        props.put("Serial No.", "0-123456789");
        props.put("Health", "100");
        props.put("Armor", "100");
        props.put("Weight", "10");
        props.put("Additional info", "For testing purposes");

        String description = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor." +
            "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. " +
            "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. " +
            "Donec pede justo, fringilla vel, aliquet nec, vulputate";

        inspector = new GCObjectInspector(layer,
            "turret1-icon", "Test Object Name", "Test Object Type", description, props);

        inspector.positionProportionally(0.7f, 0.5f);

    }

    @Override
    public void open() {
        inspector.open();
    }

    @Override
    public void close() {
        inspector.close();
    }

    @Override
    public boolean isOpen() {
        return inspector.isOpen();
    }
}
