package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.gamecomponents.client.dialogs.GCTradeDialog;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class TradeDialogDemo extends Demo {
	private GCTradeDialog tradeDialog;

	public TradeDialogDemo (H5ELayer layer) {
		tradeDialog = new GCTradeDialog(
				layer,
				"Player X",
				"Player Y",
				new InventoryV2Demo.GCInventoryDemoImpl(layer, 12),
				new InventoryV2Demo.GCInventoryDemoImpl(layer, 12)
		);
		tradeDialog.positionProportionally(0.7f, 0.5f);
	}

	@Override
	public void open() {
		tradeDialog.open();
	}

	@Override
	public void close() {
		tradeDialog.close();
	}

	@Override
	public boolean isOpen() {
		return tradeDialog.isOpen();
	}
}
