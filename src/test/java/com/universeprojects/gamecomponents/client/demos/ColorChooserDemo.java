package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.gamecomponents.client.dialogs.GCColorChooser;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class ColorChooserDemo extends Demo {

    private GCColorChooser colorChooser;

    public ColorChooserDemo(H5ELayer layer) {
        colorChooser = new GCColorChooserImpl(layer);
        colorChooser.positionProportionally(0.55f, 0.35f);
    }

    @Override
    public void open() {
        colorChooser.open();
    }

    @Override
    public void close() {
        colorChooser.close();
    }

    @Override
    public boolean isOpen() {
        return colorChooser.isOpen();
    }

    static class GCColorChooserImpl extends GCColorChooser {

        private final Logger log = Logger.getLogger(ColorChooserDemo.class);

        public GCColorChooserImpl(H5ELayer layer) {
            super(layer);
        }

        @Override
        protected int getDefaultRed() {
            return 10;
        }

        @Override
        protected int getDefaultGreen() {
            return 60;
        }

        @Override
        protected int getDefaultBlue() {
            return 120;
        }

        @Override
        public void onCancel() {
            log.info("Cancel clicked");
        }

        @Override
        protected void onOk(int r, int g, int b) {
            log.info("Ok clicked with input (R:" + r + ", G:" + g + ", B:" + b + ")");
        }

    }

}
