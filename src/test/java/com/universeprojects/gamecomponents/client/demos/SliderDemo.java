package com.universeprojects.gamecomponents.client.demos;

import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.elements.GCSlider;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.Position;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;

public class SliderDemo extends Demo {

    private final GCSimpleWindow window;

    private final GCSlider liveSlider;

    private float liveValue1, liveDelta1, liveFactor1;

    public SliderDemo(H5ELayer layer) {
        this.window = new GCSimpleWindow(layer, "slider-demo", "Slider Demo", 400, 450, false);
        window.positionProportionally(.65f, .35f);

        window.addH2("Default configuration:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        window.addElement(new GCSlider(layer), Position.NEW_LINE, Alignment.LEFT, 0, 0, true);

        window.addEmptyLine(25);
        window.addH2("Custom dimensions:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        GCSlider slider2 = window.addElement(new GCSlider(layer), Position.NEW_LINE, Alignment.LEFT, 0, 0, true);
        slider2.setWidth(350);
        slider2.setHeight(50);
        slider2.setValue(33);

        window.addEmptyLine(40);
        window.addH2("With an input box attached:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        H5EInputBox input3 = window.addInputBox(75, Position.SAME_LINE, Alignment.RIGHT, 10, 0);
        input3.setAlignment(Align.right);
        GCSlider slider3 = window.addElement(new GCSlider(layer), Position.NEW_LINE, Alignment.LEFT, 0, 0, true);
        slider3.setWidth(350);
        slider3.attachInputBox(input3);
        slider3.setValue(67.00f);

        input3.setText("" + slider3.getValue());

        window.addEmptyLine(25);
        window.addH2("Live modification:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        H5EInputBox input4 = window.addInputBox(75, Position.SAME_LINE, Alignment.RIGHT, 10, 0);
        input4.setAlignment(Align.right);
        GCSlider slider4 = window.addElement(new GCSlider(layer), Position.NEW_LINE, Alignment.LEFT, 0, 0, true);
        slider4.setWidth(350);
        slider4.setRange(0, 1000, 0.1f);
        slider4.attachInputBox(input4);
        liveSlider = slider4;

        liveValue1 = 1000;
        liveDelta1 = 0.031f;
        liveFactor1 = 1 - liveDelta1;

    }

    @Override
    public void open() {
        window.open();
    }

    @Override
    public void close() {
        window.close();
    }

    @Override
    public boolean isOpen() {
        return window.isOpen();
    }

    @Override
    public void animate() {
        liveValue1 *= liveFactor1;
        if (liveValue1 <= 100 && liveFactor1 < 1) {
            liveValue1 = 100;
            liveFactor1 += 2 * liveDelta1;
        } else if (liveValue1 >= 950 && liveFactor1 > 1) {
            liveValue1 = 950;
            liveFactor1 -= 2 * liveDelta1;
        }
        liveSlider.setValue(liveValue1);
    }
}
