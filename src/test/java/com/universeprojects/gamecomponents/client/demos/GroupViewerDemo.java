package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.GCGroupOptionsScreen;
import com.universeprojects.gamecomponents.client.dialogs.GCGroupViewer;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.Position;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

public class GroupViewerDemo extends Demo {

    private final Logger log = Logger.getLogger(GroupViewerDemo.class);


    private final GCSimpleWindow controlWindow;
    private final H5EButton btnOpenAsAdmin;
    private final H5EButton btnOpenAsNonAdmin;

    private final GCGroupViewer groupViewer;

    private boolean isAdmin = false;

    public GroupViewerDemo(H5ELayer layer) {

        controlWindow = new GCSimpleWindow(layer, "group-viewer-demo", "Group Viewer Demo");
        controlWindow.onClose.registerHandler(this::cleanup);
        controlWindow.positionProportionally(.4f, .2f);

        groupViewer = new GroupViewerDemoImpl(layer);
        groupViewer.attachGroupOptionsScreen(new GroupOptionsSreenDemoImpl(layer, groupViewer));
        groupViewer.positionProportionally(.4f, .6f);

        btnOpenAsAdmin = H5EButton.createRectBacked(layer, "Open as admin");
        btnOpenAsAdmin.addButtonListener(() -> {
            groupViewer.close();
            isAdmin = true;
            groupViewer.open();
            controlWindow.activate();
        });
        controlWindow.addElement(btnOpenAsAdmin, Position.SAME_LINE, Alignment.LEFT, 0, 0);

        btnOpenAsNonAdmin = H5EButton.createRectBacked(layer, "Open as non-admin");
        btnOpenAsNonAdmin.addButtonListener(() -> {
            groupViewer.close();
            isAdmin = false;
            groupViewer.open();
            controlWindow.activate();
        });
        controlWindow.addElement(btnOpenAsNonAdmin, Position.SAME_LINE, Alignment.LEFT, 275, 0);

    }

    @Override
    public void open() {
        controlWindow.open();
        groupViewer.open();
    }

    @Override
    public void close() {
        controlWindow.close();
    }

    private void cleanup() {
        groupViewer.close();
    }

    @Override
    public boolean isOpen() {
        return controlWindow.isOpen();
    }

    private class GroupViewerDemoImpl extends GCGroupViewer {

        protected GroupViewerDemoImpl(H5ELayer layer) {
            super(layer);
        }

        @Override
        protected GCGroupViewerContent refreshContent() {
            GCGroupViewerContent content = new GCGroupViewerContent();
            content.groupName = "The Intergalactic Alliance";
            content.groupDescription = "The Intergalactic Alliance (Shorthand: The Alliance) is a multi-faction conglomerate, " +
                "united by the goal of military and economic domination of the Realm.";

            content.userName = "Xonak";
            content.isAdmin = GroupViewerDemo.this.isAdmin;
            content
                .addMember("Xonak", null)
                .addMember("S'Anral", null)
                .addMember("Kora Veka", null)
                .addMember("Issac Quintan", null)
                .addMember("Ch'Keeda", null)
                .addMember("Wes Grippen", null)
                .addMember("Ri'Mang", null)
                .addMember("Sataur", null);

            return content;
        }

        @Override
        protected String getSelectionName() {
            return "Bozo";
        }

        @Override
        protected void doLeaveGroup(String groupName) {
            log.info("Leave group: " + groupName);
        }

        @Override
        protected void doInvite(String groupName, String entityName) {
            log.info("Invite entity " + Strings.inQuotes(entityName) + " into group " + Strings.inQuotes(groupName));
        }

        @Override
        protected void doKick(String groupName, String entityName) {
            log.info("Kick entity " + Strings.inQuotes(entityName) + " out of group " + Strings.inQuotes(groupName));
        }
    }

    private class GroupOptionsSreenDemoImpl extends GCGroupOptionsScreen {

        public GroupOptionsSreenDemoImpl(H5ELayer layer, GCGroupViewer groupViewer) {
            super(layer, groupViewer);
        }

        @Override
        protected boolean refreshContent() {
            return false;
        }

        @Override
        protected void doUpdate(String groupName, String desc) {
            log.info("Update group: name=" + Strings.inQuotes(groupName) + ", description=" + Strings.inQuotes(desc));
        }

        @Override
        protected void doCloseGroup(String groupName) {
            log.info("Close group: name=" + Strings.inQuotes(groupName));
        }
    }

}
