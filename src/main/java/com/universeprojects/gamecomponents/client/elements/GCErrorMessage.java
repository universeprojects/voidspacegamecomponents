package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.graphics.Color;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ERectangle;

public class GCErrorMessage extends H5ERectangle {

    private final H5ELabel errorMessage;

    public GCErrorMessage(H5ELayer layer) {
        super(layer);
        setColor(Color.valueOf("#FF5555"));
        setWidth(0);
        setHeight(25);
        setY(5);
        setRoundedCornerRadius(2);

        errorMessage = new H5ELabel(layer);
        add(errorMessage);
        errorMessage.setFontScale(0.6f);
        errorMessage.setColor(Color.valueOf("#EEEEEE"));
        errorMessage.setX(5);
        errorMessage.setY(2);
    }

    public void clearError() {
        error(null);
    }

    public void error(String errorMsg) {
        if (Strings.isEmpty(errorMsg)) {
            errorMessage.setText("");
            setWidth(0);
            setVisible(false);
        } else {
            errorMessage.setText(errorMsg);
            setWidth(errorMessage.getWidth() + 12);
            setVisible(true);
        }
    }

}