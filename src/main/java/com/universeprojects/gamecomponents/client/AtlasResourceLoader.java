package com.universeprojects.gamecomponents.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.universeprojects.html5engine.client.framework.H5EAudio;
import com.universeprojects.html5engine.client.framework.H5EImage;
import com.universeprojects.html5engine.client.framework.ResourceLoader;

import java.util.Collection;

public class AtlasResourceLoader implements ResourceLoader {

    private final TextureAtlas atlas;

    public AtlasResourceLoader(TextureAtlas atlas) {
        this.atlas = atlas;
    }

    @Override
    public void loadResources(Collection<H5EImage> imageFiles, Collection<H5EAudio> audioFiles) {
        for (H5EImage image : imageFiles) {
            String url = image.getOriginalUrl();
            Sprite texture = getSprite(url);
            image.setTexture(texture);
        }
        for (H5EAudio audio : audioFiles) {
            Sound sound = Gdx.audio.newSound(Gdx.files.internal(audio.getURL()));
            audio.setSound(sound);
        }
    }

    protected Sprite getSprite(String url) {
        if (url.startsWith("images/")) {
            url = url.substring("images/".length());
        }
        if (url.endsWith(".jpg")) {
            url = url.substring(0, url.length() - ".jpg".length());
        } else if (url.endsWith(".jpeg")) {
            url = url.substring(0, url.length() - ".jpeg".length());
        } else if (url.endsWith(".png")) {
            url = url.substring(0, url.length() - ".png".length());
        }
        return atlas.createSprite(url);
    }

    @Override
    public H5EImage loadSingleImage(String imageUrl) {
        Sprite sprite = getSprite(imageUrl);
        if(sprite == null) {
            return null;
        }
        H5EImage image = new H5EImage(imageUrl);
        image.setTexture(sprite);
        return image;
    }

    @Override
    public String getDefaultSpriteKey() {
        return "unknown-icon";
    }
}
