package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.universeprojects.common.shared.util.DevException;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCRecipeData.GCRecipeSlotDataItem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;

import java.util.List;

public abstract class GCSlotItemSelector {

    private final H5EScrollablePane slotListContainer;
    private GCList<GCRecipeSlotListItem> slotList;
    private GCRecipeSlotListItem.GCRecipeSlotListHandler slotListHandler;

    private final H5EScrollablePane optionsListContainer;
    private GCList<GCExperimentsListItem> optionsList;
    private GCListItemActionHandler<GCExperimentsListItem> optionsListHandler;

    private GCRecipeData data;
    private Integer selectedSlotIdx;
    public static final int LIST_SPACING = 5;

    private final H5ELayer layer;

    public GCSlotItemSelector(H5ELayer layer, H5EScrollablePane slotListContainer, H5EScrollablePane optionsListContainer) {
        this.layer = layer;

        this.slotListContainer = slotListContainer;
        this.optionsListContainer = optionsListContainer;

        slotListHandler = new GCRecipeSlotListItem.GCRecipeSlotListHandler() {

            @Override
            public void onSlotDeselected() {
                selectedSlotIdx = null;
                clearSlotOptionsList();
            }

            @Override
            public void onSlotSelected(GCRecipeSlotDataItem slot) {
                selectedSlotIdx = slot.index;
                if (!slot.isItemSelected()) {
                    populateSlotOptionsData(slot.getItems());
                }
            }

            @Override
            public void onDeselectOptionBtnPressed(GCRecipeSlotDataItem slot) {
                GCInventoryDataItem itemToDeselect = slot.getSelectedItem();
                if (itemToDeselect == null) {
                    throw new DevException("Unexpected: de-select button is only available when an item is selected");
                }
                slot.clearSelectedItem();
                selectedSlotIdx = slot.index;

                boolean dataReload = onSlotItemDeselected(slot, itemToDeselect);
                if (!dataReload) {
                    populateSlotData(data, slot.index, true);
                }
            }

            @Override
            public void onInfoButtonClicked(GCRecipeSlotDataItem slot) {
                onSlotInfoClicked(slot.getSelectedItem());
            }
        };

        optionsListHandler = new GCListItemActionHandler<GCExperimentsListItem>() {
            @Override
            protected void onSelectionUpdate(GCExperimentsListItem lastSelectedItem) {

                GCRecipeSlotDataItem slot = data.getSlot(selectedSlotIdx);
                if (slot.isItemSelected()) {
                    throw new DevException("Unexpected: option selection is only available when the slot is empty");
                }

                GCExperimentsListItem selectedOption = this.getSelectedItem();
                if (selectedOption == null) {
                    throw new DevException("Unexpected: options list should only be visible for selection, not for de-selection");
                }

                slot.selectItem(selectedOption.data.uid);
                GCInventoryDataItem selectedItem = slot.getSelectedItem();

                boolean dataReload = onSlotItemSelected(slot, selectedItem);
                if (!dataReload) {
                    populateSlotData(data, selectedSlotIdx, true);
                    clearSlotOptionsList();
                }
            }

            @Override
            public void onInfoButtonClicked(GCInventoryDataItem item) {
                onSlotInfoClicked(item);
            }
        };

        clear();
    }

    public Integer getSelectedSlotIdx() {
        return selectedSlotIdx;
    }

    public GCRecipeData getData() {
        return data.copy();
    }

    public void init(GCRecipeData data, boolean restoreScrollPosition, Integer slotIndex) {
        this.data = data.copy();
        clearSlotOptionsList();
        populateSlotData(this.data, slotIndex, restoreScrollPosition);
    }

    public void update(GCRecipeData data) {
        List<GCRecipeSlotDataItem> existingSlots = this.data.getSlots();
        List<GCRecipeSlotDataItem> incomingSlots = data.getSlots();
        if (existingSlots.size() != incomingSlots.size()) {
            throw new IllegalStateException("The number of slots is different");
        }
        for (int i = 0; i < existingSlots.size(); i++) {
            GCRecipeSlotDataItem incomingSlot = data.getSlot(i);
            GCRecipeSlotDataItem existingSlot = this.data.getSlot(i);
            existingSlot.replaceItems(incomingSlot.getItems());
        }

        clearSlotOptionsList();
        populateSlotData(this.data, selectedSlotIdx, true);
    }

    public void clear() {
        data = GCRecipeData.createNew(GCRecipeData.RecipeMode.ITEM_SKILL, -1L); // default empty
        clearSlotList();
        clearSlotOptionsList();
    }

    private void clearSlotList() {
        if (slotList != null) {
            slotListHandler.reset();
            slotList.clear();
            slotList.remove();
            slotList = null;
            slotListContainer.getContent().clear();
        }
    }

    private void clearSlotOptionsList() {
        if (optionsList != null) {
            optionsListHandler.reset();
            optionsList.clear();
            optionsList.remove();
            optionsList = null;
            optionsListContainer.getContent().clear();
        }
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    private void populateSlotData(GCRecipeData recipeData, Integer selectedSlotIdx, boolean restoreScrollPosition) {
        float scrollPosition = restoreScrollPosition ? slotListContainer.getScrollPosition() : 0;

        clearSlotList();
        if (recipeData == null) {
            return;
        }

        GCRecipeSlotListItem itemToSelect = null;
        slotList = new GCList<>(layer, 1, LIST_SPACING, LIST_SPACING, true);
        for (int index = 0; index < recipeData.size(); index++) {
            GCRecipeSlotDataItem dataItem = recipeData.getSlot(index);
            GCRecipeSlotListItem listItem = new GCRecipeSlotListItem(layer, slotListHandler, dataItem);
            slotList.addItem(listItem);
            if (selectedSlotIdx != null && selectedSlotIdx == index) {
                itemToSelect = listItem;
            }
        }

        slotListContainer.add(slotList).top().left().grow();
        slotListContainer.scrollToPosition(scrollPosition);
        if (itemToSelect != null) {
            slotListHandler.updateSelection(itemToSelect);
        }
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    private void populateSlotOptionsData(GCInventoryData slotOptionsData) {
        clearSlotOptionsList();
        if (slotOptionsData == null) {
            return;
        }

        optionsList = new GCList<>(layer, 1, LIST_SPACING, LIST_SPACING, true);
        for (int index = 0; index < slotOptionsData.size(); index++) {
            GCInventoryDataItem dataItem = slotOptionsData.get(index);
            GCExperimentsListItem listItem = new GCExperimentsListItem(layer, optionsListHandler, dataItem);
            optionsList.addItem(listItem);
        }

        optionsListContainer.add(optionsList).top().left().grow();
    }

    /**
     * @return TRUE if the control data will be re-loaded as a result of this operation
     */
    protected abstract boolean onSlotItemSelected(GCRecipeSlotDataItem slot, GCInventoryDataItem selectedItem);

    /**
     * @return TRUE if the control data will be re-loaded as a result of this operation
     */
    protected abstract boolean onSlotItemDeselected(GCRecipeSlotDataItem slot, GCInventoryDataItem deselectedItem);

    protected abstract void onSlotInfoClicked(GCInventoryDataItem item);


}
