package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCIdeasData.GCIdeaDataItem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

class GCInventionIdeasTab extends GCInventionTab {

    private final H5EScrollablePane listContainer;
    private GCList<GCIdeaListItem> ideasList;
    private final GCListItemActionHandler<GCIdeaListItem> listHandler;
    public static final int LIST_SPACING = 5;

    protected GCInventionIdeasTab(GCInventionSystemDialog dialog) {
        super(dialog, "Ideas");

        listContainer = rightContent.add(new H5EScrollablePane(layer)).left().top().grow().getActor();
        listContainer.getContent().top().left();

        listHandler = new GCListItemActionHandler<GCIdeaListItem>() {
            @Override
            protected void onSelectionUpdate(GCIdeaListItem lastSelectedItem) {
                // do nothing
            }
        };

        final H5EButton btnBeginPrototyping = ButtonBuilder.inLayer(layer).withStyle("button2").withText("Begin Prototyping").build();
        lowerButtonRow.add(btnBeginPrototyping).grow();
        btnBeginPrototyping.addButtonListener(() -> {
            GCIdeaListItem selectedIdea = listHandler.getSelectedItem();
            Long selectedId = selectedIdea == null ? null : selectedIdea.data.id;
            dialog.onBeginPrototypingBtnPressed(selectedId);
        });
    }

    @Override
    protected void clearTab() {
        if (ideasList != null) {
            listHandler.reset();
            ideasList.clear();
            ideasList.remove();
            ideasList = null;
            listContainer.getContent().clear();
        }
    }

    void populateData(GCIdeasData ideasData) {
        clearTab();

        ideasList = new GCList<>(layer, 1, LIST_SPACING, LIST_SPACING, true);
        ideasList.left().top();
        listContainer.add(ideasList).left().top().grow();
        for (int index = 0; index < ideasData.size(); index++) {
            GCIdeaDataItem dataItem = ideasData.get(index);
            GCIdeaListItem listItem = new GCIdeaListItem(layer, listHandler, dataItem);
            ideasList.addItem(listItem);
        }
    }

}
