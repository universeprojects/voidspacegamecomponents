package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

/**
 * Class for setting buttons as drag and drop targets.
 * When a button is dragged over, the button set as a target is clicked.
 */
public class TabButtonTarget extends DragAndDrop.Target {

    public TabButtonTarget(Actor actor) {
        super(actor);
    }

    /**
     * Activates button click upon drag over. Unused parameters.
     */
    @Override
    public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        ((H5EButton) getActor()).setChecked(true);
        return false;
    }

    @Override
    public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {

    }
}
