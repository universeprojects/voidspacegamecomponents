package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.elements.GCLabel;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCBasicListItem extends GCListItem {

    private final GCLabel label;

    public GCBasicListItem(H5ELayer layer, GCListItemActionHandler<GCBasicListItem> actionHandler, int width, int height, String caption) {
        super(layer, actionHandler);

        label = add(new GCLabel(layer)).getActor();
        label.setFontScale(0.6f);
        label.setColor(Color.valueOf("#CCCCCC"));
        label.setX(5);
        label.setY(5);
        label.setTouchable(Touchable.disabled);

        if (Strings.isEmpty(caption)) {
            throw new IllegalArgumentException("Caption text can't be blank");
        }
        label.setText(caption);
    }

    public String getCaption() {
        return label.getText().toString();
    }


}
