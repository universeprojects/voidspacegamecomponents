package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCRecipeData.GCRecipeSlotDataItem;
import com.universeprojects.gamecomponents.client.elements.GCLabel;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;


@SuppressWarnings("FieldCanBeLocal")
public class GCRecipeSlotListItem extends GCListItem {

    public static final int INFO_ICON_SIZE = 19;
    public static final int INFO_ICON_OFFSET = 2;

    protected final GCRecipeSlotDataItem data;

    private final H5ELabel slotDescLabel;
    private final Button itemContainer;
    private final H5EIcon itemIcon;
    private final H5EIcon itemInfoIcon;
    private final H5ELabel quantityLabel;
    private final H5ELabel itemNameLabel;
    private final H5EButton clearSlotBtn;

    final static int HEIGHT = 80;
    final static int ITEM_HEIGHT = 55;
    public static final int ICON_SIZE = 64;


    public GCRecipeSlotListItem(H5ELayer layer, GCRecipeSlotListHandler handler, GCRecipeSlotDataItem data) {
        super(layer, handler);

        if (data.index < 0) {
            throw new IllegalArgumentException("Index can't be negative");
        }
        this.data = data;

        String desc = data.name;
        if (data.required != null && !data.required) {
            desc += " (optional)";
        }
        slotDescLabel = add(new H5ELabel(layer)).growX().getActor();
        slotDescLabel.setText(desc);
        slotDescLabel.setColor(Color.valueOf("#CCCCCC"));
        slotDescLabel.setTouchable(Touchable.disabled);

        row();

        if (!data.isItemSelected()) {
            itemContainer = null;
            itemIcon = null;
            itemInfoIcon = null;
            quantityLabel = null;
            itemNameLabel = null;
            clearSlotBtn = null;
        } else {
            GCInventoryDataItem selectedOption = data.getSelectedItem();
            itemContainer = add(new Button(getEngine().getSkin(), "gc-list-item")).growX().getActor();
            itemContainer.left().top();
            itemContainer.defaults().left().top();

            H5EStack stack = new H5EStack(layer);
            itemContainer.add(stack).size(ICON_SIZE).padRight(5).padLeft(20).size(ICON_SIZE);

            itemIcon = H5EIcon.fromSpriteType(layer, selectedOption.iconSpriteKey);
            stack.add(itemIcon);
            itemIcon.addListener(new ClickListener() {
                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    handler.onInfoButtonClicked(data);
                    event.handle();
                }
            });

            itemInfoIcon = H5EIcon.fromSpriteType(layer, "inspect-indicator", Scaling.none, Align.topRight);
            stack.add(itemInfoIcon);

            Table subTable = new Table().left().top();
            subTable.defaults().left().top();
            itemContainer.add(subTable).grow();

            if (selectedOption.quantity == 1) {
                quantityLabel = null;
            } else {
                quantityLabel = subTable.add(new H5ELabel(layer)).left().padRight(10).getActor();
                quantityLabel.setText(Integer.toString(selectedOption.quantity));
                quantityLabel.setColor(Color.valueOf("#FFFFFF"));
            }

            itemNameLabel = subTable.add(new GCLabel(layer)).growX().getActor();
            itemNameLabel.setText(selectedOption.name);
            itemNameLabel.setColor(Color.valueOf("#FFFFFF"));
            itemNameLabel.setTouchable(Touchable.disabled);

            subTable.row();

            clearSlotBtn = subTable.add(ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Clear").build()).colspan(2).width(120).growY().getActor();
            clearSlotBtn.setColor(Color.valueOf("#CCCCCC"));

            clearSlotBtn.addButtonListener(() -> handler.onDeselectOptionBtnPressed(data));
        }

    }

    static abstract class GCRecipeSlotListHandler extends GCListItemActionHandler<GCRecipeSlotListItem> {

        void updateSelection(GCRecipeSlotListItem item) {
            super.setSelection(item);
        }

        @Override
        protected final void onSelectionUpdate(GCRecipeSlotListItem lastSelectedItem) {
            GCRecipeSlotListItem selectedItem = getSelectedItem();
            if (selectedItem == null) {
                onSlotDeselected();
            } else {
                onSlotSelected(selectedItem.data);
            }
        }

        public abstract void onSlotDeselected();

        public abstract void onSlotSelected(GCRecipeSlotDataItem slot);

        public abstract void onDeselectOptionBtnPressed(GCRecipeSlotDataItem slot);

        public abstract void onInfoButtonClicked(GCRecipeSlotDataItem slot);
    }

}
