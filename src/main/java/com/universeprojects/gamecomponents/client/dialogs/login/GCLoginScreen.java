package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.Input;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.shared.UPUtils;

/**
 * Creates the UI for players to login.
 */
public class GCLoginScreen<L extends LoginInfo> extends GCLoginInformation {

    private final GCLoginManager<L, ?, ?> loginManager;

    private final GCLoadingIndicator loadingIndicator;

    public GCLoginScreen(H5ELayer layer, GCLoginManager<L, ?, ?> loginManager) {
        super(layer);
        this.loginManager = loginManager;
        loadingIndicator = new GCLoadingIndicator(layer);
        window.setTitle("Sign In");
        window.setCloseButtonEnabled(false);
        //Add "email" text
        window.row().padTop(10);
        window.add(emailLabel);

        //Space to enter email
        window.row().padTop(10);
        window.add(fieldEmail).width(LOGIN_FIELD_W);

        //Warning text for invalid emails
        window.row().padTop(5);
        window.add(emailWarningText);

        //Add "password" text
        window.row().padTop(5);
        window.add(passwordLabel);

        //Space to enter password
        window.row().padTop(10);
        window.add(fieldPassword).width(LOGIN_FIELD_W);

        //Warning text for invalid passwords
        window.row().padTop(10);
        window.add(passwordWarningText);

        //Login button
        window.row().padTop(10);
        final H5EButton btnLogin = new H5EButton("login", layer);
        btnLogin.addButtonListener(this::submitLoginInfo);
        btnLogin.getStyle().checked=null;
        window.add(btnLogin).size(200, btnLogin.getHeight() ).getActor();

        //Register Button
        window.row().padTop(5);
        final H5EButton btnRegister = new H5EButton("Register", layer);
        btnRegister.addButtonListener(this::openRegisterWindow);
        btnRegister.getStyle().checked=null;
        window.add(btnRegister).size(200, btnLogin.getHeight() ).getActor();
        //Get default colour from text fields

        //Tie both fields to enter key
        UPUtils.tieButtonToKey(fieldEmail, Input.Keys.ENTER, btnLogin);
        UPUtils.tieButtonToKey(fieldPassword, Input.Keys.ENTER, btnLogin);
    }

    private void submitLoginInfo(){
        emailWarningText.setVisible(false);
        passwordWarningText.setVisible(false);

        fieldEmail.setColor(normalColor);
        fieldPassword.setColor(normalColor);


        if(!isValidEmail()) {
            return;
        }
        if(!isValidPassword()){
            return;
        }
        processLogin(getEmail(), getPassword());
    }

    public void onLoginError() {
        passwordWarningText.setText("Error communicating with the server. Please try again later.");
        passwordWarningText.setVisible(true);
    }

    public void onLoginFailed() {
        passwordWarningText.setText("Your email or password is incorrect. Please try again.");
        passwordWarningText.setVisible(true);
        fieldPassword.setColor(warningColor);
        fieldEmail.setColor(warningColor);
    }

    private void processLogin(String email, String password) {
        loadingIndicator.activate(window);
        loginManager.login(email, password, (loginInfo) -> {
            loadingIndicator.deactivate();
            close();
            loginManager.onSuccessfulLogin(loginInfo);
        }, (error) -> {
            loadingIndicator.deactivate();
            if(GCLoginManager.CODE_LOGIN_FAILED.equals(error)) {
                onLoginFailed();
            } else {
                onLoginError();
            }
        });
    }

    private void openRegisterWindow() {
        close();
        loginManager.openRegisterWindowWithPolicies();
    }

}
