package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Pools;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.Map;

public class GCItemInspector<K, T extends GCInventoryItem> extends GCWindow {

    private final GCItemIcon<T> itemIcon;
    private final H5ELabel objectNameLabel;
    private final H5ELabel objectTypeLabel;
    private final H5ELabel descriptionTextArea;
    private final Table parameterTable;
    private final Table additionalComponentsTable;
    private T item;

    private boolean positionNextToSlot = false;
    protected GCInventorySlotComponent<K, T, ?> currentComponent;
    public static final float CONTENT_FONT_SIZE = 0.8f;
    public static final float TITLE_FONT_SIZE = 1f;

    public GCItemInspector(H5ELayer layer) {
        super(layer, 120, 200, "inspector-window");
        left().top();
        defaults().left().top();
        padRight(10);
        itemIcon = new GCItemIcon<>(getLayer());

        getTitleLabel().setVisible(false);

        add(itemIcon).size(64);

        final Table nameTable = new Table();
        nameTable.left().top();
        nameTable.defaults().left().top();
        add(nameTable).growX().padLeft(5);
        objectNameLabel = new H5ELabel(getLayer());
        objectNameLabel.setFontScale(TITLE_FONT_SIZE);
        objectNameLabel.setAlignment(Align.left);
        nameTable.add(objectNameLabel).padTop(10).left().growX();

        nameTable.row();
        objectTypeLabel = new H5ELabel(getLayer());
        objectTypeLabel.setFontScale(CONTENT_FONT_SIZE);
        objectTypeLabel.setAlignment(Align.left);
        nameTable.add(objectTypeLabel).padTop(2).padLeft(25).growX();

        row();

        descriptionTextArea = new H5ELabel(getLayer());
        descriptionTextArea.setWrap(true);
        descriptionTextArea.setFontScale(CONTENT_FONT_SIZE);
        add(descriptionTextArea).colspan(2).fillX();

        row();

        parameterTable = new Table();
        parameterTable.left().top();
        add(parameterTable).fill().colspan(2).padLeft(5);

        row();

        additionalComponentsTable = new Table();
        additionalComponentsTable.left().top();
        add(additionalComponentsTable).fill().colspan(2);

        row();
    }

    public boolean isPositionNextToSlot() {
        return positionNextToSlot;
    }

    public void setPositionNextToSlot(boolean positionNextToSlot) {
        this.positionNextToSlot = positionNextToSlot;
    }

    public void setSlotComponent(GCInventorySlotComponent<K, T, ?> component) {
        // place the icon on top of the invisible placeholder
        final T item = component.getItem();
        itemIcon.setItem(item);

        this.currentComponent = component;

        objectNameLabel.setText(item.getName());
        objectTypeLabel.setText(item.getItemClass());

        final String description = item.getDescription();
        descriptionTextArea.setVisible(!description.isEmpty());
        descriptionTextArea.setText(description);

        setParameters(item.getAdditionalProperties());
        item.createAdditionalInspectorComponents(additionalComponentsTable);

        onSetSlotComponent(item, component);
        setItem(item);

        pack();
        if(isPositionNextToSlot()) {
            positionForSlot(component);
        } else {
            positionForBaseInventory(component);
        }
        toFront();
        activate();
    }

    /**
     * Resets the inspector's view of the item's fields and recreates the additional components
     */
    public void redrawInspector() {
        objectNameLabel.setText(item.getName());
        objectTypeLabel.setText(item.getItemClass());

        final String description = item.getDescription();
        descriptionTextArea.setVisible(!description.isEmpty());
        descriptionTextArea.setText(description);

        setParameters(item.getAdditionalProperties());
        item.createAdditionalInspectorComponents(additionalComponentsTable);
    }

    protected void onSetSlotComponent(T item, GCInventorySlotComponent<K, T, ?> component) {
    }

    public void setItem(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }

    public Table getAdditionalComponentsTable() {
        return additionalComponentsTable;
    }

    protected void positionForBaseInventory(GCInventorySlotComponent<?, T, ?> component) {
        Actor actor = component.inventoryBase;
        Vector2 vec = Pools.obtain(Vector2.class);
        vec.set(0, actor.getHeight());
        actor.localToStageCoordinates(vec);
        setPosition(vec.x, vec.y, Align.topRight);
        Pools.free(vec);
    }

    protected void positionForSlot(GCInventorySlotComponent<K, T, ?> component) {
        Vector2 vec = Pools.obtain(Vector2.class);
        vec.set(0, 0);
        component.localToStageCoordinates(vec);
        setPosition(vec.x, vec.y, Align.bottomRight);
        Pools.free(vec);
    }

    public void setParameters(Map<String, String> parameters) {
        parameterTable.clearChildren();
        parameterTable.setVisible(!parameters.isEmpty());

        for(Map.Entry<String, String> entry : parameters.entrySet()) {
            parameterTable.row().padBottom(2);
            final String keyText = entry.getKey();
            final String valueText = entry.getValue();
            final H5ELabel keyLabel = new H5ELabel(keyText, getLayer());
            final H5ELabel valueLabel = new H5ELabel(valueText, getLayer());
            keyLabel.setFontScale(CONTENT_FONT_SIZE);
            valueLabel.setFontScale(CONTENT_FONT_SIZE);
            keyLabel.setAlignment(Align.left);
            valueLabel.setAlignment(Align.left);
            parameterTable.add(keyLabel).left();
            parameterTable.add(valueLabel).growX().padLeft(5).left();
        }
    }
}
