package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCSkillsData.GCSkillDataItem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

@SuppressWarnings("FieldCanBeLocal")
class GCInventionItemsTab extends GCInventionTab {

    private final H5EScrollablePane listContainer;
    private GCList<GCItemsListItem> skillsList;
    private final GCListItemActionHandler<GCItemsListItem> listHandler;
    private final H5EButton btnBuildItem;
    private final H5EButton btnDeleteSkill;
    public static final int LIST_SPACING = 5;

    protected GCInventionItemsTab(GCInventionSystemDialog dialog) {
        super(dialog, "Item construction");

        listContainer = rightContent.add(new H5EScrollablePane(layer)).left().top().grow().getActor();
        listContainer.getContent().top().left();

        listHandler = new GCListItemActionHandler<GCItemsListItem>() {
            @Override
            protected void onSelectionUpdate(GCItemsListItem lastSelectedItem) {
                // do nothing
            }
        };


        btnBuildItem = ButtonBuilder.inLayer(layer).withStyle("button2").withText("Build").build();
        btnBuildItem.addButtonListener(() -> {
            GCItemsListItem selectedSkill = listHandler.getSelectedItem();
            Long selectedId = selectedSkill == null ? null : selectedSkill.data.id;
            dialog.onBuildItemBtnPressed(selectedId);
        });
        lowerButtonRow.add(btnBuildItem).grow().uniform();

        btnDeleteSkill = ButtonBuilder.inLayer(layer).withStyle("button-red").withText("Forget").build();
        btnDeleteSkill.addButtonListener(() -> {
            GCItemsListItem selectedSkill = listHandler.getSelectedItem();
            Long selectedId = selectedSkill == null ? null : selectedSkill.data.id;
            dialog.onDeleteSkillBtnPressed(selectedId);
        });
        lowerButtonRow.add(btnDeleteSkill).grow().uniform();
    }

    @Override
    protected void clearTab() {
        if (skillsList != null) {
            listHandler.reset();
            skillsList.clear();
            skillsList.remove();
            skillsList = null;
            listContainer.getContent().clear();
        }
    }

    void populateData(GCSkillsData itemsData) {
        clearTab();

        skillsList = new GCList<>(layer, 1, LIST_SPACING, LIST_SPACING, true);
        skillsList.left().top();
        listContainer.add(skillsList).left().top().grow();
        for (int index = 0; index < itemsData.size(); index++) {
            GCSkillDataItem dataItem = itemsData.get(index);
            GCItemsListItem listItem = new GCItemsListItem(layer, listHandler, dataItem);
            skillsList.addItem(listItem);
        }
    }

}
