package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCSkillsData.GCSkillDataItem;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

class GCItemsListItem extends GCListItem {

    public static final int ICON_SIZE = 64;
    final GCSkillDataItem data;

    private final H5EIcon icon;
    private final H5ELabel nameLabel;
    private final H5ELabel descTextArea;

    public GCItemsListItem(H5ELayer layer, GCListItemActionHandler<GCItemsListItem> actionHandler, GCSkillDataItem data) {
        super(layer, actionHandler);

        this.data = Dev.checkNotNull(data);
        if (data.index < 0) {
            throw new IllegalArgumentException("Index can't be negative");
        }
        if (Strings.isEmpty(data.name)) {
            throw new IllegalArgumentException("Skill name can't be blank");
        }
        if (Strings.isEmpty(data.description)) {
            throw new IllegalArgumentException("Skill description can't be blank");
        }
        if (Strings.isEmpty(data.iconKey)) {
            throw new IllegalArgumentException("Skill icon key can't be blank");
        }

        icon = add(H5EIcon.fromSpriteType(layer, data.iconKey)).size(ICON_SIZE).getActor();
        icon.setTouchable(Touchable.disabled);

        Table subTable = new Table();
        add(subTable).growX().fillY();
        subTable.left().top();
        subTable.defaults().left().top();

        nameLabel = subTable.add(new H5ELabel(layer)).getActor();
        nameLabel.setFontScale(1f);
        nameLabel.setColor(Color.valueOf("#FAFAFA"));
        nameLabel.setText(data.name);
        nameLabel.setTouchable(Touchable.disabled);

        subTable.row();

        descTextArea = subTable.add(new H5ELabel(layer)).growX().fillY().getActor();
        descTextArea.setFontScale(0.8f);
        descTextArea.setColor(Color.valueOf("#CCCCCC"));
        descTextArea.setText(data.description);
        descTextArea.setWrap(true);
        descTextArea.setTouchable(Touchable.disabled);
    }

}
