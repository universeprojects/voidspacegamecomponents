package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCEquipmentInspector<S extends Comparable<S>, T extends GCInventoryItem> extends GCItemInspector<EquipmentSlotKey<S>, T> {

    public GCEquipmentInspector(H5ELayer layer) {
        super(layer);
        Table table = add(new Table()).colspan(2).fill().getActor();
    }
}
