package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCRecipeData.GCRecipeSlotDataItem;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;


class GCInventionRecipeTab extends GCInventionTab {

    private final GCSlotItemSelector selector;
    private final H5EInputBox txtRepeats;

    GCInventionRecipeTab(GCInventionSystemDialog dialog) {
        super(dialog, "Select the tools/materials to use");

        final H5EScrollablePane slotListContainer = new H5EScrollablePane(layer);
        final H5EScrollablePane optionsListContainer = new H5EScrollablePane(layer);

        leftContent.add(slotListContainer).top().left().grow().colspan(2);
        rightContent.add(optionsListContainer).top().left().grow();

        slotListContainer.getContent().top().left();
        optionsListContainer.getContent().top().left();

        selector = new GCSlotItemSelector(layer, slotListContainer, optionsListContainer) {
            @Override
            protected boolean onSlotItemSelected(GCRecipeSlotDataItem slot, GCInventoryDataItem selectedItem) {
                return false;
            }

            @Override
            protected boolean onSlotItemDeselected(GCRecipeSlotDataItem slot, GCInventoryDataItem deselectedItem) {
                return false; // no data reload
            }

            @Override
            protected void onSlotInfoClicked(GCInventoryDataItem item) {
                GCInventionRecipeTab.this.onSlotInfoClicked(item);
            }


        };

        leftContent.row().padBottom(5);

        final H5ELabel txtRepeatsLabel = leftContent.add(new H5ELabel(layer)).fill().getActor();
        txtRepeatsLabel.setText("Repeats: ");

        txtRepeats = leftContent.add(new H5EInputBox(layer)).fillY().growX().getActor();
        txtRepeats.setTypeNumber();
        txtRepeats.setAlignment(Align.right);
        txtRepeats.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                txtRepeats.selectAll();
            }
        });
        resetRepeats();

        final H5EButton btnStart = ButtonBuilder.inLayer(layer).withStyle("button2").withText("Start").build();
        btnStart.addButtonListener(() -> {
            int repeats = 1;
            try {
                repeats = Integer.parseInt(txtRepeats.getText());
            } catch (NumberFormatException e) {
                //Ignore
            }
            dialog.onRecipeOkayBtnPressed(selector.getData(), repeats);
        });
        lowerButtonRow.add(btnStart).grow();

        UPUtils.tieButtonToKey(txtRepeats, Input.Keys.ENTER, btnStart);
    }

    /**
     * Reset the repeats input box
     */
    public void resetRepeats () {
        txtRepeats.setText("1");
        txtRepeats.selectAll();
        txtRepeats.focus();
    }

    @Override
    protected void clearTab() {
        selector.clear();
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);

        if (visible) {
            txtRepeats.selectAll();
            txtRepeats.focus();
        }
    }

    void initRecipeData(GCRecipeData recipeData) {
        selector.init(recipeData, false, null);
    }

    void updateRecipeData(GCRecipeData recipeData) {
        selector.update(recipeData);
    }
}
