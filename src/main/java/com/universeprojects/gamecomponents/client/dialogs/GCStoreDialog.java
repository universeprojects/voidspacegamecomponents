package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.ArrayList;
import java.util.List;

public class GCStoreDialog extends GCSimpleWindow {
    private List<GCStoreOrderComponent> storeOrderComponentsList;
    private H5EScrollablePane storeScrollView;

    public GCStoreDialog (H5ELayer layer) {
        super(layer, "storeDialogWindow", "Store Dialog", 800, 600, false);
        storeOrderComponentsList = new ArrayList<>();
        storeScrollView = new H5EScrollablePane(layer, "scrollpane-backing-1");

        Table headerTable = new Table();
        H5ELabel sellLabel = new H5ELabel("Selling", layer);
        sellLabel.setFontScale(1.2f);
        H5ELabel buyLabel = new H5ELabel("Buying", layer);
        buyLabel.setFontScale(1.2f);

        headerTable.add(sellLabel).left().grow().width(Value.percentWidth(0.45f, headerTable));
        headerTable.add(buyLabel).left().grow().width(Value.percentWidth(0.45f, headerTable));
        // Empty placeholder to conform to formatting of the scroll pane
        headerTable.add().width(Value.percentWidth(0.1f, headerTable));
        this.add(headerTable).growX();

        this.row();
        this.add(storeScrollView).grow();
        storeScrollView.getContent().left().top();
    }

    //TODO allow player named store
    public GCStoreDialog (H5ELayer layer, String storeName) {
        super(layer, "storeDialogWindow", storeName);
        storeOrderComponentsList = new ArrayList<>();

        this.add("Selling").left().grow();
        this.add("Buying").right().grow();
    }

    /**
     * Adds a store order component to the dialog.
     */
    public void addStoreOrderToDialog (GCStoreOrderComponent order) {
        GCStoreOrderComponent newOrderComponent = new GCStoreOrderComponent(
                getLayer(), order.getStoreOrderKey(),
                order.getBuyItemName(), order.getBuyItemIconName(), order.getBuyItemQuantity(),
                order.getSellItemName(), order.getSellItemIconName(), order.getSellItemQuantity()) {

            @Override
            public void startTransaction(String storeOrderKey) {
                doTransaction(storeOrderKey);
            }
        };

        storeOrderComponentsList.add(newOrderComponent);
        storeScrollView.getContent().row();

        newOrderComponent.addStoreEntry(storeScrollView.getContent(), newOrderComponent.getBuyItemName(), newOrderComponent.getBuyItemIconName(), newOrderComponent.getBuyItemQuantity());
        newOrderComponent.addStoreEntry(storeScrollView.getContent(), newOrderComponent.getSellItemName(), newOrderComponent.getSellItemIconName(), newOrderComponent.getSellItemQuantity());
        newOrderComponent.addStoreBuyButton(storeScrollView.getContent());
    }

    public void clearOrdersFromDialog() {
        this.storeOrderComponentsList.clear();
        this.storeScrollView.getContent().clearChildren();
    }

    public void update () {
        //TODO update the store dialog
    }

    /**
     * Method to be overridden when a StoreDialog is created.
     * Used startTransaction when a new store order is added to the dialog.
     */
    public void doTransaction (String storeOrderKey) {

    }
}
