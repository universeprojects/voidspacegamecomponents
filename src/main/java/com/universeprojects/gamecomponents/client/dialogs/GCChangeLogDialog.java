package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.util.TextBuilder;
import com.universeprojects.gamecomponents.client.common.UIFactory;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;

import java.util.List;

public class GCChangeLogDialog {
	private GCSimpleWindow window;
	private H5EScrollablePane scrollablePane;
	private GCLoadingIndicator loadingIndicator;

	private static final String[] COLOR_LOOKUP_TABLE = {
			"#CCCCCC",  //default
			"#0066FF",  //code-low
			"#0066FF",  //code-medium
			"#3399FF",  //code-high
			"#009933",  //content-low
			"#009933",  //content-medium
			"#00FF00"   //content-high
	};

	public GCChangeLogDialog (H5ELayer layer) {
		final int WIDTH = 300;
		final int HEIGHT = 400;

		loadingIndicator = new GCLoadingIndicator(layer);

		window = new GCSimpleWindow(layer, "changeLogWindow", "VoidSpace Change Log", WIDTH, HEIGHT, true);
		scrollablePane = new H5EScrollablePane(window.getLayer(), "scrollpane-backing-1");

		window.add(scrollablePane)
				.width(window.getWidth())
				.height(window.getHeight())
				.align(Align.left);

		window.pack();

	}

	public void open() {
		window.positionProportionally(.5f, .5f);
		window.open();
	}

	public void close() {
		window.close();
	}

	public void destroy() {
		window.destroy();
	}

	public boolean isOpen() {
		return window.isOpen();
	}

	public void activate() {
		window.activate();
	}

	public void positionProportionally(float propX, float propY) {
		window.positionProportionally(propX, propY);
	}

	public void flagLoading() {
		loadingIndicator.activate(window);
	}

	public void clear () {
		scrollablePane.getContent().clear();
	}

	/**
	 * Add an entry to the changelog
	 * @param gameVersion
	 * @param interest
	 * @param timestamp
	 * @param type
	 * @param logItems
	 */
	public void addChangeLogEntry (String gameVersion, String interest, List<String> logItems, String timestamp, String type) {
		final int ENTRY_SPACING = 5;

		//Add text to label

		TextBuilder text = new TextBuilder();

		text.appendLine(gameVersion);

		for (String logItem : logItems) {
			text.appendLine("- " + logItem);
		}

		//Style the labels

		float fontScale = 1.0f;
		int colorIndex = 0;

		if (type.equalsIgnoreCase("Code")) {
			colorIndex += 1;
		} else if (type.equalsIgnoreCase("Content")) {
			colorIndex += 4;
		}

		if (interest.equalsIgnoreCase("Low")) {
			fontScale = 0.7f;
			colorIndex += 0;
		} else if (interest.equalsIgnoreCase("Medium")) {
			fontScale = 0.9f;
			colorIndex += 1;
		} else if (interest.equalsIgnoreCase("High")) {
			fontScale = 1.0f;
			colorIndex += 2;
		}

		new UIFactory(scrollablePane.getContent(), window.getLayer())
				.addLabel(text.toString(), fontScale, COLOR_LOOKUP_TABLE[colorIndex], Align.left, true)
				.addEmptyLine(ENTRY_SPACING);
	}

	/**
	 * Stop displaying the loading icon
	 */
	public void finishLoading () {
		TextBuilder text = new TextBuilder();

		for (int i = 0; i < 20; i++) {
			text.appendLine();
		}

		text.appendLine(" Congratulations! You've found an easter egg!");

		new UIFactory(scrollablePane.getContent(), window.getLayer())
				.addLabel(text.toString());

		loadingIndicator.deactivate();
	}
}
