package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

abstract class GCInventionTab {

    protected final GCInventionSystemDialog dialog;
    protected final H5ELayer layer;

    protected final Table lowerButtonRow;
    protected final Table leftContent;
    protected final Table rightContent;
    protected final Table rightTitleArea;

    protected GCInventionTab(GCInventionSystemDialog dialog, String title) {
        this.dialog = dialog;
        lowerButtonRow = new Table();
        dialog.lowerButtonRow.add(lowerButtonRow);
        lowerButtonRow.defaults().left().top();
        lowerButtonRow.left().top();

        leftContent = new Table();
        leftContent.defaults().left().top();
        leftContent.left().top();
        leftContent.setFillParent(true);
        dialog.leftContent.add(leftContent);

        rightContent = new Table();
        rightContent.defaults().left().top();
        rightContent.left().top();
        dialog.rightContent.add(rightContent);

        rightTitleArea = new Table();
        rightTitleArea.defaults().left().top();
        rightTitleArea.left().top();
        dialog.rightTitleArea.add(rightTitleArea);

        this.layer = dialog.window.getLayer();
        final H5ELabel titleLabel = new H5ELabel(layer);
        rightTitleArea.add(titleLabel).growX();
        titleLabel.setColor(Color.valueOf("#FFFFFF"));
        titleLabel.setFontScale(1.2f);
        titleLabel.setText(title);

        hide(); // start hidden
    }

    void show() {
        setVisible(true);
    }

    void hide() {
        setVisible(false);
    }

    public void setVisible(boolean visible) {
        leftContent.setVisible(visible);
        rightTitleArea.setVisible(visible);
        rightContent.setVisible(visible);
        lowerButtonRow.setVisible(visible);
    }

    void deactivate() {
        clearTab();
        hide();
    }

    public void onSlotInfoClicked(GCInventoryData.GCInventoryDataItem item) {
        dialog.onIconInfoClicked(item.uid);
    }

    abstract protected void clearTab();

    //Click button when a key is pressed

}
