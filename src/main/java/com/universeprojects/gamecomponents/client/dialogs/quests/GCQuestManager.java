package com.universeprojects.gamecomponents.client.dialogs.quests;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.dialogs.Confirmable;
import com.universeprojects.gamecomponents.client.dialogs.GCConfirmationScreen;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.List;
import java.util.Map;

public class GCQuestManager extends GCSimpleWindow implements Confirmable {
	private static final float BUTTON_FONT_SCALE = 0.8f;

	private enum QuestDeactivationType {
		SKIP,
		ABANDON
	}

	//Sidebar
	private final GCQuestSidebar sidebar;

	private GCQuestListWrapper quests;
	private GCList<GCQuestListItem> questListPanel;
	private ButtonGroup questButtonGroup;
    private final H5ELayer layer;

	//Description side assets
	private H5ELabel questDescriptionLabel;
	private H5ELabel questEmptyWarning;

	private H5EButton btnAbandonSelectedQuest;
	private H5EButton btnSkipSelectedQuest;

	private GCConfirmationScreen confirmationScreen;
	private QuestDeactivationType questDeactivationType = null;	//Enum for tracking the quest deactivation type
	private final GCListItemActionHandler<GCQuestListItem> questHandler;

	public GCQuestManager(H5ELayer layer, int width, int height, GCQuestSidebar sidebar) {
		super(layer, "quest-manager", "Quest Manager", width, height, false);
		this.layer = layer;
        this.sidebar = sidebar;
        defaults().left().top();

		// Top row of buttons ------------------------------------------------------------------------------------------

		//Quest buttons
		H5EButton btnActiveQuest    = createQuestButton(layer, "Active Quest",     BUTTON_FONT_SCALE, 2, this::refresh);
		H5EButton btnCompletedQuest = createQuestButton(layer, "Completed Quests", BUTTON_FONT_SCALE, 2, this::refreshDisabled);
		H5EButton btnSkippedQuest   = createQuestButton(layer, "Skipped Quests",   BUTTON_FONT_SCALE, 2, this::refreshDisabled);
		H5EButton btnAbandonedQuest = createQuestButton(layer, "Abandoned Quests", BUTTON_FONT_SCALE, 2, this::refreshDisabled);

		btnActiveQuest.setChecked(true);

		//Make button group with the quest buttons
		questButtonGroup = new ButtonGroup<>(btnActiveQuest, btnCompletedQuest, btnSkippedQuest, btnAbandonedQuest);
		questButtonGroup.setMaxCheckCount(1);
		questButtonGroup.setMinCheckCount(0);
		questButtonGroup.setUncheckLast(true);
		row();

		//Create action handler for quest list items
		questHandler = new GCListItemActionHandler<GCQuestListItem>() {
			@Override
			protected void onSelectionUpdate(GCQuestListItem lastSelectedItem) {
			GCQuestListItem selectedItem = getSelectedItem();
			if (selectedItem == null) return;
			questDescriptionLabel.setText(selectedItem.description + "\n\n" + listObjectives(selectedItem));
			}
		};

		// Left panel (quest list) -------------------------------------------------------------------------------------

		//Quest list scroll pane
        H5EScrollablePane leftScrollPane = new H5EScrollablePane(layer, "scrollpane-backing-2");
		leftScrollPane.setScrollingDisabled(false,false);

		//Add a warning label for an empty quest list to the scroll pane
		questEmptyWarning = new H5ELabel("", layer);
		questEmptyWarning.setAlignment(Align.center);
		questEmptyWarning.setFontScale(0.7f);
		leftScrollPane.add(questEmptyWarning).align(Align.center);
		leftScrollPane.getContent().row();

		//Add quests
		quests = new GCQuestListWrapper();
		sidebar.link(quests.getActiveQuests());

		//Add the quest list panel to the scroll pane
		questListPanel = new GCList<>(layer, 1, 5, 5, false);
		leftScrollPane.add(questListPanel).growX().top();
		leftScrollPane.getContent().align(Align.top);

		//Add the current quests to the list panel
		for(GCQuestListItem currentQuest: quests.getActiveQuests()){
			questListPanel.addItem(currentQuest);
		}

		//Add the scroll pane to the window
		add(leftScrollPane).colspan(3).uniformX().grow();

		// Right panel (description) -----------------------------------------------------------------------------------

		//Quest description right scrollpane
		H5EScrollablePane rightScrollPane = new H5EScrollablePane(layer, "scrollpane-backing-2");
		rightScrollPane.setScrollingDisabled(false,false);

		//quest description label
		questDescriptionLabel = new H5ELabel("", layer);
		questDescriptionLabel.setWrap(true);
		questDescriptionLabel.setAlignment(Align.topLeft);
		questDescriptionLabel.setFontScale(BUTTON_FONT_SCALE);

		//Button to actually skip a quest (bottom right buttons)
		btnSkipSelectedQuest = new H5EButton("Skip Quest", layer);
		btnSkipSelectedQuest.getLabel().setFontScale(BUTTON_FONT_SCALE);
		btnSkipSelectedQuest.addButtonListener(() -> openQuestDeactivationDialog(QuestDeactivationType.SKIP));

		//Button to actually abandon a quest
		btnAbandonSelectedQuest = new H5EButton("Abandon Quest", layer);
		btnAbandonSelectedQuest.getLabel().setFontScale(BUTTON_FONT_SCALE);
		btnAbandonSelectedQuest.addButtonListener(() -> openQuestDeactivationDialog(QuestDeactivationType.ABANDON));

		//Add buttons for skipping and abandoning quests
		Table table = new Table();
		table.add(questDescriptionLabel).grow().row();
		table.add(btnSkipSelectedQuest).height(30).row();
		table.add(btnAbandonSelectedQuest).height(30);
		rightScrollPane.add(table).top().left().grow().padLeft(5);
		add(rightScrollPane).colspan(5).uniformX().grow();

		//Create the confirmation dialog for skipping/abandoning quests
		confirmationScreen = new GCConfirmationScreen(
			layer,
			this,
			"Are you sure you want to skip this quest?"
		);

		close();
	}

	/**
	 * Add a button with the specified properties
	 * @param label        The label for the button
	 * @param fontScale    The size of the font
	 * @param colspan      The number of columns that the button should span
	 * @param buttonAction The function to be called when the button is clicked
	 * @return A button with the specified properties
	 */
	private H5EButton createQuestButton (H5ELayer layer, String label, float fontScale, int colspan, Callable0Args buttonAction) {
		H5EButton button = new H5EButton(label, layer);
		button.getLabel().setFontScale(fontScale);
		add(button).colspan(colspan).uniformX().center().growX();
		button.addButtonListener(buttonAction);
		return button;
	}

	/**
	 * Open the quest manager in the middle of the screen
	 */
	@Override
	public void open () {
		refresh();
		positionProportionally(0.5f, 0.5f);
		super.open();
	}

	/**
	 * Add a quest to the quest manager
	 */
	public void addQuest (String name, String description, Map<String, GCQuestObjective> objectiveMap, QuestState questState) {
		//Create a new quest graphical component
		GCQuestListItem newQuestItem = new GCQuestListItem(layer, questHandler, name, description, objectiveMap, questState);

		//Add the quest to a different list depending on its state
		switch (questState) {
			case Incomplete:
				quests.getActiveQuests().add(newQuestItem);
				break;
			case Complete:
				quests.getCompletedQuests().add(newQuestItem);
				break;
			case Abandoned:
				quests.getAbandonedQuests().add(newQuestItem);
				break;
			case Skipped:
				quests.getSkippedQuests().add(newQuestItem);
				break;
		}
	}

	/**
	 * Clears The current listing. Used when listings are changed to prevent errors from
	 * adding and removing elements
	 */

	private void clearQuestPanel (){
		questListPanel.clear();
		questListPanel.getButtonGroup().clear();
	}

	/**
	 * Clears and displays the quest listing based which listing you're trying to view with the selected button(active, completed
	 * abandoned etc)
	 */
	private void refreshQuestPanel (){
		List<GCQuestListItem> questListItems;
		boolean disableSkipButtons;

		if (questButtonGroup == null) return;
		int checkedIndex = questButtonGroup.getCheckedIndex();

		switch (checkedIndex) {
			case 0: //Active quests
				questListItems = quests.getActiveQuests();
				//These buttons are disabled until the feature is implemented
//				disableSkipButtons = false;
				disableSkipButtons = true;
				break;
			case 1:	//Completed quests
				questListItems = quests.getCompletedQuests();
				disableSkipButtons = true;
				break;
			case 2: //Skipped quests
				questListItems = quests.getSkippedQuests();
				disableSkipButtons = true;
				break;
			case 3: //Abandoned quests
				questListItems = quests.getAbandonedQuests();
				disableSkipButtons = true;
				break;
			default:
				return;
		}

		clearQuestPanel();
		for(GCQuestListItem currentQuest: questListItems){
			questListPanel.addItem(currentQuest);
		}

		setDisabledSkipButtons(disableSkipButtons);
		emptyQuestText();
	}

	/**
	 * Clear all the currently stored quests
	 */
	public void clearQuests () {
		quests.getActiveQuests().clear();
		quests.getCompletedQuests().clear();
		quests.getAbandonedQuests().clear();
		quests.getSkippedQuests().clear();
	}

	/**
	 * Disable/enable the skip and abandon buttons. Typically used while the user is looking at completed/skipped/abandoned quest.
	 * @param disabled True if you want to disable the buttons. Otherwise pass false to enable them.
	 */
	private void setDisabledSkipButtons (boolean disabled) {
		btnSkipSelectedQuest.setDisabled(disabled);
		btnAbandonSelectedQuest.setDisabled(disabled);
	}

	/**
	 * Display warning text if there are no quests
	 */
	private void emptyQuestText () {
		if(questListPanel.size() < 1){
			questEmptyWarning.setText("No quest available");
		} else {
			questEmptyWarning.setText("");
		}
	}

	/**
	 * Open the dialog to deactivate (i.e., skip or abandon) a quest
	 * @param questDeactivationType The type of quest deactivation (skip or abandon)
	 */
	private void openQuestDeactivationDialog(QuestDeactivationType questDeactivationType) {
		//A quest can only be abandoned if a quest is selected
		if (!isButtonSelected()) return; //TODO: Make this some kind of warning

		this.questDeactivationType = questDeactivationType;
		confirmationScreen.setText("Are you sure you want to " + questDeactivationType.toString() + " this quest?");
		confirmationScreen.setTitle(questDeactivationType.toString() + " Quest");
		confirmationScreen.open();
		this.close();
	}

	/**
	 * @return Whether there is a selected quest
	 */
	private boolean isButtonSelected(){
		return questListPanel.getButtonGroup().getCheckedIndex() != -1;
	}

	/**
	 * This method is called after the user confirms or denies to skip/abandon quest. Used To call the
	 * appropriate method after confirmation
	 */
	@Override
	public void confirmed () {
		if (!confirmationScreen.isConfirmed()) return;

		switch (questDeactivationType) {
			case SKIP:
				skipQuest();
				break;
			case ABANDON:
				abandonQuest();
				break;
			default:
				return;
		}

		this.open();
	}

	/**
	 * Refresh the quest dialogs
	 */
	public void refresh () {
		refreshQuestPanel();
		refreshDescriptionText();
		sidebar.refresh();
	}

	/**
	 * Refresh the quest dialogs for the quests that are not active
	 */
	private void refreshDisabled () {
		clearQuestPanel();
		questEmptyWarning.setText("This feature is not available yet");
		refreshDescriptionText();
		sidebar.refresh();
	}

	/**
	 * Refresh the text in the description
	 */
	private void refreshDescriptionText(){
		if (questHandler == null) return;

		//Make sure that the quest list has quests in it
		List<GCQuestListItem> selectedQuestList = getSelectedQuestList();
		if (selectedQuestList == null || selectedQuestList.size() == 0) {
			questDescriptionLabel.setText("");
			return;
		}

		//Get the selected quest
		int checkedIndex = questListPanel.getButtonGroup().getCheckedIndex();
		GCQuestListItem selectedItem = questListPanel.get(checkedIndex);

		if (selectedItem == null) {
			questDescriptionLabel.setText("");
		} else {
			questDescriptionLabel.setText(selectedItem.description + "\n\n" + listObjectives(selectedItem));
		}
	}

	/**
	 * Get the currently open quest list based on the checked button
	 * @return A list of the quests in the currently opened quest list
	 */
	private List<GCQuestListItem> getSelectedQuestList () {
		List<GCQuestListItem> selectedQuestList = null;

		if (questListPanel == null) return null;
		int checkedIndex = questListPanel.getButtonGroup().getCheckedIndex();

		switch (checkedIndex) {
			case 0:
				selectedQuestList = quests.getActiveQuests();
				break;
			case 1:
				selectedQuestList = quests.getCompletedQuests();
				break;
			case 2:
				selectedQuestList = quests.getSkippedQuests();
				break;
			case 3:
				selectedQuestList = quests.getAbandonedQuests();
				break;
		}

		return selectedQuestList;
	}

	/**
	 * Create a string listing the objectives for a quest
	 * @param selectedItem The quest to display objectives for
	 * @return A string listing the objectives for a quest
	 */
	private static String listObjectives (GCQuestListItem selectedItem) {
		StringBuilder message = new StringBuilder();

		for (GCQuestObjective questObjective : selectedItem.objectiveMap.values()) {
			message.append("  - ").append(questObjective.getName()).append(": ");
			message.append(questObjective.getCurrentCompletion()).append(" / ").append(questObjective.getRequiredCompletion()).append('\n');

			//Append the objective's hint
//			if (!questObjective.getHint().isEmpty()) {
//				message.append("     - ").append(questObjective.getHint()).append('\n');
//			}

			message.append('\n');
		}

		return message.toString();
	}

	/**
	 * Skip the currently selected quest
	 */
	public void skipQuest() {
		moveQuest(quests.getActiveQuests(), quests.getSkippedQuests());
	}

	/**
	 * Abandon the currently selected quest
	 */
	public void abandonQuest() {
		moveQuest(quests.getActiveQuests(), quests.getAbandonedQuests());
	}

	/**
	 * Move a quest from one list to another
	 * @param sourceList The list to get the quest from
	 * @param targetList The list to move the quest to
	 */
	private void moveQuest (List<GCQuestListItem> sourceList, List<GCQuestListItem> targetList) {
		int checkedIndex = questListPanel.getButtonGroup().getCheckedIndex();
		GCQuestListItem selectedQuest = sourceList.get(checkedIndex);
		targetList.add   (selectedQuest);
		sourceList.remove(selectedQuest);
		refresh();
	}
}
