package com.universeprojects.gamecomponents.client.dialogs.quests;

/**
 * All the possible quest states
 */
public enum QuestState {
    Skipped,
    Incomplete,
    Complete,
    Abandoned
}
