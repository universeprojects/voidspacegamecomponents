package com.universeprojects.gamecomponents.client.dialogs.quests;

import com.universeprojects.common.shared.util.Dev;

public class GCQuestObjective {
    /**
     * A string representing the key for the objective
     */
    private String objectiveKey;

    /**
     * The name of the objective
     */
    private String name;

    /**
     * A hint for completing the objective
     */
    private String hint;

    /**
     * The required "done-ness" of the objective
     */
    private double requiredCompletion;

    /**
     * The current "done-ness" of the objective
     */
    private double currentCompletion;

    public GCQuestObjective (String objectiveKey, String name, String hint, double requiredCompletion, double currentCompletion) {
        this.objectiveKey = Dev.checkNotNull(objectiveKey);
        this.name         = (name == null) ? ("") : (name);
        this.hint         = (hint == null) ? ("") : (hint);
        this.requiredCompletion = requiredCompletion;
        this.currentCompletion  = currentCompletion;
    }

    public String getObjectiveKey() {
        return objectiveKey;
    }

    public String getName() {
        return name;
    }

    public String getHint() {
        return hint;
    }

    public double getRequiredCompletion() {
        return requiredCompletion;
    }

    public double getCurrentCompletion() {
        return currentCompletion;
    }

    public boolean isComplete () {
        return currentCompletion >= requiredCompletion;
    }
}
