package com.universeprojects.gamecomponents.client.dialogs;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class GCInventoryData {

    private final Map<Integer, GCInventoryDataItem> itemsByIndex = new HashMap<>();
    private final Map<String, GCInventoryDataItem> itemsByUid = new HashMap<>();

    private GCInventoryData() {
    }

    public static GCInventoryData createNew() {
        return new GCInventoryData();
    }

    public GCInventoryData copy() {
        GCInventoryData copy = new GCInventoryData();
        itemsByIndex.values().forEach((item) -> copy.add(item.uid, item.name, item.iconSpriteKey, item.quantity));
        return copy;
    }

    public GCInventoryData add(GCInventoryData other) {
        for (GCInventoryDataItem item : other.itemsByIndex.values()) {
            // we call the add() method sequentially in order for new indices to be assigned
            add(item.uid, item.name, item.iconSpriteKey, item.quantity);
        }
        return this;
    }

    public GCInventoryData add(String uid, String name, String iconSpriteKey, int quantity) {
        int index = itemsByIndex.size();
        return add(new GCInventoryDataItem(index, uid, name, iconSpriteKey, quantity));
    }

    public GCInventoryData add(String uid, String name, String iconSpriteKey, int quantity, boolean selectable) {
        int index = itemsByIndex.size();
        return add(new GCInventoryDataItem(index, uid, name, iconSpriteKey, quantity, selectable));
    }

    public GCInventoryData add(GCInventoryDataItem item) {
        if (itemsByIndex.containsKey(item.index)) {
            throw new IllegalStateException("Duplicate index: " + item.index);
        }
        if (itemsByUid.containsKey(item.uid)) {
            throw new IllegalStateException("Duplicate UID: " + item.uid);
        }
        itemsByIndex.put(item.index, item);
        itemsByUid.put(item.uid, item);
        return this;
    }

    public int size() {
        return itemsByIndex.size();
    }

    public GCInventoryDataItem get(int index) {
        return itemsByIndex.get(index);
    }

    public GCInventoryDataItem getByUid(String uid) {
        return itemsByUid.get(uid);
    }

    public boolean hasUid(String uid) {
        return itemsByUid.containsKey(uid);
    }

    public Set<String> getUids() {
        return new HashSet<>(itemsByUid.keySet());
    }

    public void replace(GCInventoryDataItem item) {
        if (!itemsByUid.containsKey(item.uid)) {
            throw new IllegalStateException("UID not found: " + item.uid);
        }
        remove(item.uid);
        add(item);
    }

    public void remove(String uid) {
        if (itemsByUid.containsKey(uid)) {
            GCInventoryDataItem oldItem = itemsByUid.remove(uid);
            itemsByIndex.remove(oldItem.index);
        }
    }

    public void removeAll(Collection<String> uids) {
        uids.forEach(this::remove);
    }

    public void forEach(Consumer<GCInventoryDataItem> consumer) {
        itemsByIndex.values().forEach(consumer);
    }

    public static class GCInventoryDataItem {
        public final int index;
        public final String uid;
        public final String name;
        public final String iconSpriteKey;
        public final int quantity;
        public final boolean selectable;

        //Default to being selectable
        GCInventoryDataItem(int index, String uid, String name, String iconSpriteKey, int quantity) {
            this(index, uid, name, iconSpriteKey, quantity, true);
        }

        GCInventoryDataItem(int index, String uid, String name, String iconSpriteKey, int quantity, boolean selectable) {
            this.index = index;
            this.uid = uid;
            this.name = name;
            this.iconSpriteKey = iconSpriteKey;
            this.quantity = quantity;
            this.selectable = selectable;
        }

        public GCInventoryDataItem copy() {
            return new GCInventoryDataItem(index, uid, name, iconSpriteKey, quantity, selectable);
        }

    }

}


