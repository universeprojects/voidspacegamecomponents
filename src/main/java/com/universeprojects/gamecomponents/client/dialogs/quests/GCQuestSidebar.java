package com.universeprojects.gamecomponents.client.dialogs.quests;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.List;

/**
 * This class describes the quest quick-view sidebar
 */
public class GCQuestSidebar extends GCWindow {
    /**
     * The name of the default style for the sidebar
     */
    private static final String DEFAULT_STYLE = "action-progress-bar-window";

    /**
     * The default width of the sidebar
     */
    private static final int DEFAULT_WIDTH = 350;

    /**
     * The default height of the sidebar
     */
    private static final int DEFAULT_HEIGHT = 200;

    /**
     * The default proportions of the screen for positioning the sidebar
     */
    private static final float DEFAULT_X_PROPORTION = 0.075f;
    private static final float DEFAULT_Y_PROPORTION = 0.5f;

    /**
     * A list of the currently displayed quests
     */
    private final GCList<GCQuestSidebarListItem> displayedQuests;

    /**
     * A reference to the list of quests that can be displayed in the sidebar
     */
    private List<GCQuestListItem> questItemList = null;

    /**
     * Constructors for the sidebar
     */
    public GCQuestSidebar(H5ELayer layer) {
        this(layer, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_STYLE);
    }

    private GCQuestSidebar(H5ELayer layer, Integer width, Integer height, String styleName) {
        super(layer, width, height, styleName);

        //Set the window's properties
        padTop(20);
        setMovable(false);
        setPackOnOpen(true);
        setTouchable(Touchable.disabled);
        setCloseButtonEnabled(false);

        //Add the list of quests to the sidebar
        row();
        displayedQuests = new GCList<>(layer, 1, 10, 5, true);
        add(displayedQuests).grow().left();

        //Start in the displayed state by default
        open();
    }

    /**
     * Set the sidebar's reference to the list of quests that can be displayed
     * @param quests The list of quests that can be displayed
     */
    public void link (List<GCQuestListItem> quests) {
        questItemList = quests;
    }

    /**
     * Refresh the quests displayed on the sidebar
     */
    public void refresh () {
        //Open the sidebar if it is not already
        if (!isOpen() || !isVisible()) {
            super.open();
            setVisible(true);
        }

        //Clear the currently displayed quests
        displayedQuests.clear();
        displayedQuests.getButtonGroup().clear();

        //Load quests to display from the reference list
        if (questItemList == null || questItemList.isEmpty()) {
            setTitle("");
            setVisible(false);
            return;
        }

        //Only display objectives for a single quest
        boolean foundQuest = false;
        for (GCQuestListItem questListItem : questItemList) {
            if (foundQuest) break;

            //Display all objectives
            setTitle(questListItem.name);
            for (GCQuestObjective objective : questListItem.objectiveMap.values()) {
                GCQuestSidebarListItem listItem = new GCQuestSidebarListItem(questListItem.layer, questListItem.actionHandler, objective);
                displayedQuests.addItem(listItem);
                foundQuest = true;
            }
        }

        positionProportionally(DEFAULT_X_PROPORTION, DEFAULT_Y_PROPORTION);
        capLeft();
    }

    /**
     * Open the sidebar
     */
    @Override
    public void open () {
        refresh();
    }

    /**
     * Overriding the close method to make sure that the sidebar never closes
     */
    @Override
    public void close () {}

    /**
     * Overriding the setVisible method to make sure that the sidebar is always visible
     */
    @Override
    public void setVisible (boolean visible) {
        super.setVisible(true);
    }
}
