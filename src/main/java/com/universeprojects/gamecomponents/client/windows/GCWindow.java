package com.universeprojects.gamecomponents.client.windows;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EWindow;
import com.universeprojects.html5engine.shared.GenericEvent;

public class GCWindow extends Window implements H5EWindow {

    /**
     * The global minimum width
     */
    private static final int WINDOW_MIN_WIDTH = 200;
    /**
     * The global minimum height
     */
    private static final int WINDOW_MIN_HEIGHT = 125;
    /**
     * The height of the title bar
     */
    private static final int TITLE_BAR_HEIGHT = 40;

    /**
     * The total amount of X-axis padding to the left of the content pane
     */
    private static final int CONTENT_PADDING_LEFT = 16;
    /**
     * The total amount of X-axis padding to the right of the content pane
     */
    private static final int CONTENT_PADDING_RIGHT = 16;
    /**
     * The total amount of Y-axis padding from the top of the content pane
     */
    private static final int CONTENT_PADDING_TOP = 8;
    /**
     * The total amount of Y-axis padding from the bottom of the content pane
     */
    private static final int CONTENT_PADDING_BOTTOM = 16;

    private static final int CONTENT_PADDING_W = CONTENT_PADDING_LEFT + CONTENT_PADDING_RIGHT;
    private static final int CONTENT_PADDING_H = CONTENT_PADDING_TOP + CONTENT_PADDING_BOTTOM;

    /**
     * The transparency value applied to active windows
     */
    static final float ACTIVE_ALPHA = 1f;
    /**
     * The transparency value applied to inactive windows
     */
    static final float INACTIVE_ALPHA = 0.85f;

    /**
     * This button is in the corner of the title bar; Clicking it closes the window
     */
    private final ImageButton btnClose;

    public final GenericEvent.GenericEvent0Args onClose = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent0Args onOpen = new GenericEvent.GenericEvent0Args();
    private boolean packOnOpen;

    private Integer level;

    public boolean isCloseButtonEnabled() {
        return btnClose.isVisible();
    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        final H5ELayer layer = getLayer();
        if(layer == null) {
            return null;
        }
        return layer.getEngine();
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public int getLevel() {
        if (level == null) return getLayer().getDefaultLevel();
        return level;
    }

    /**
     * This keeps track of all the input boxes in the window (they take a bit of special care)
     */
    public GCWindow(H5ELayer layer, Integer width, Integer height) {
        this(layer, width, height, "default");
    }

    public GCWindow(H5ELayer layer, Integer width, Integer height, String styleName) {
        super("", layer.getEngine().getSkin(), styleName);
        GCWindowStyle style = (GCWindowStyle) getStyle();
        layer.addElement(this, level);
        layer.addToTop(this);
        final Label titleLabel = getTitleLabel();
        titleLabel.setAlignment(Align.center);
        final Table titleTable = getTitleTable();
        if (style.titleBackground != null) {
            titleLabel.getStyle().background = style.titleBackground;
        }
        final Cell<Label> labelCell = titleTable.getCell(titleLabel);
        labelCell.padBottom(style.labelPadBottom).padLeft(style.labelPadLeft);
        labelCell.fill(false, false);
        setClip(false);
        setTransform(true);
        setKeepWithinStage(false);

        width = capMin(width, (int) getBackground().getMinWidth());
        height = capMin(height, (int) getBackground().getMinHeight());

        /*
      Stores the last seen screen width (for window resizing purposes)
     */

        addCaptureListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!getWindowManager().isActive(GCWindow.this)) {
                    getWindowManager().activateWindow(GCWindow.this);
//                    event.setBubbles(false);
//                    return true;
//                } else {
                }
                return false;
            }
        });
        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                event.handle();
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                event.handle();
                super.touchDragged(event, x, y, pointer);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                event.handle();
                super.touchUp(event, x, y, pointer, button);
            }

            @Override
            public void clicked(InputEvent event, float x, float y) {
                event.handle();
                super.clicked(event, x, y);
            }
        });

        setWidth(width);
        setHeight(height);

        btnClose = new ImageButton(getSkin(), "btn-close-window");
        btnClose.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                close(true);
                event.handle();
            }
        });
        final Cell<ImageButton> btnCell = titleTable.add(btnClose);
        btnCell.padRight(style.closeButtonPadRight).padTop(style.closeButtonPadTop);

        // do this so that the boundaries check takes effect
        setX(getX());
        setY(getY());

        // start hidden
        setupInactiveState();
        setVisible(false);
    }

    private static int capMin(Integer value, int min) {
        return value == null ? min : UPMath.capMin(value, min);
    }

//        private final H5EEventHandler eventHandler = new H5EEventHandler() {
//        @Override
//        public void handleEvent(H5EEvent event) {
//            if (event == H5EButton.onButtonUnpressEvent) {
//                if (event.getSource() == btnClose) {
//                    close(true);
//                }
//            } else if (event == H5EGraphicElement.onPointerDownEvent) {
//                if (event.getSource() == inactiveOverlay || event.getSource() == titleBar) {
//                    GCWindowManager.getInstance().activateWindow(GCWindow.this);
//                }
//            } else if (event == H5EEngine.onResizeEvent) {
//                // maintain the proportion of window coordinates in relation to screen dimensions
//                float propX = getX() / screenWidth;
//                float propY = getY() / screenHeight;
//                int newScreenW = engine.getWidth();
//                int newScreenH = engine.getHeight();
//                setX(newScreenW * propX);
//                setY(newScreenH * propY);
//                screenWidth = newScreenW;
//                screenHeight = newScreenH;
//            }
//        }
//    };

    //    /**
//     * Internal helper method
//     */
//    private static void applyTransparencyRecursively(H5EGraphicElement element, float newVal) {
//        Dev.checkNotNull(element);
//        Dev.check(newVal == ACTIVE_ALPHA || newVal == INACTIVE_ALPHA);
//
//        // if this is a container, recursively apply to children first
//        if (element instanceof H5EContainer) {
//            H5EContainer container = (H5EContainer) element;
//            if (container.hasChildren()) {
//                for (H5EGraphicElement child : container.getChildren()) {
//                    applyTransparencyRecursively(child, newVal);
//                }
//            }
//        }
//
//        // finally, apply the value to this particular element
//
//        float curVal = element.getTransparency();
//        if (element.getTransparencyFactor() == 1.0) {
//            // Fallback to this legacy correction mechanism when a "transparency factor" setting is not defined!
//            //
//            // This will attempt to proportionally scale the element's custom transparency value... it is an unreliable approach.
//            if (newVal == ACTIVE_ALPHA && curVal != INACTIVE_ALPHA) {
//                newVal = curVal * (ACTIVE_ALPHA / INACTIVE_ALPHA);
//            } else if (newVal == INACTIVE_ALPHA && curVal != ACTIVE_ALPHA) {
//                newVal = curVal * (INACTIVE_ALPHA / ACTIVE_ALPHA);
//            }
//        }
//        element.setTransparency(newVal);
//    }
//
//    /**
//     * Internal framework method. DO NOT MAKE PUBLIC!
//     * <p>
//     * Updates the visual state of the window to reflect that it is "active"
//     */
    final void setupActiveState() {
        setColor(new Color(1f, 1f, 1f, ACTIVE_ALPHA));
    }

    //        if (getTransparency() == ACTIVE_ALPHA) {
//            return; // already setup
//        }
//
//        // visual effect
//        applyTransparencyRecursively(window, ACTIVE_ALPHA);
//
//        // make the window jump above all other windows
//        // TODO: this is not reliable, because the windows are not guaranteed to be in the same layer (need to enforce that)
//        layer.moveToForeground(window);
//
//        // the overlay becomes transparent to let pointer events through
//        inactiveOverlay.setTouchable(Touchable.disabled);
//
//        // enable the input boxes, if there are any
//        if (inputBoxes != null) {
//            for (H5EInputBox inputBox : inputBoxes) {
//                inputBox.enable();
//            }
//        }
//
//    }
//
//    /**
//     * Internal framework method. DO NOT MAKE PUBLIC!
//     * <p>
//     * Updates the visual state of the window to reflect that it is "inactive"
//     */
    final void setupInactiveState() {
        setColor(new Color(1f, 1f, 1f, INACTIVE_ALPHA));
    }
//        if (getTransparency() == INACTIVE_ALPHA) {
//            return; // already setup
//        }
//
//        // visual effect
//        applyTransparencyRecursively(window, INACTIVE_ALPHA);
//
//        // all pointer events will be caught by the overlay
//        inactiveOverlay.setTouchable(Touchable.enabled);
//
//        // disable the input boxes, if there are any
//        if (inputBoxes != null) {
//            for (H5EInputBox inputBox : inputBoxes) {
//                inputBox.disable();
//            }
//        }
//    }
//
//    public void setContentWidth(int width) {
//        setWidth(width + CONTENT_PADDING_W);
//    }
//
//    public void setContentHeight(int height) {
//        setHeight(height + TITLE_BAR_HEIGHT + CONTENT_PADDING_H);
//    }

    @Override
    public void setWidth(int width) {
        width = UPMath.capMin(width, WINDOW_MIN_WIDTH);

        super.setWidth(width);
    }

    @Override
    public void setHeight(int height) {
        height = UPMath.capMin(height, WINDOW_MIN_HEIGHT);

        super.setHeight(height);
    }

    @Override
    public void setOriginCenter() {
        setOrigin(getWidth() / 2f, getHeight() / 2f);
    }

    @Override
    public void setId(String id) {
        setName(id);
    }

    @Override
    public String getId() {
        return getName();
    }

    @Override
    public void setTitle(String title) {
        getTitleLabel().setText(Strings.nullToEmpty(title).toUpperCase());
    }

    @Override
    public void setCloseButtonEnabled(boolean newVal) {
        btnClose.setVisible(newVal);
    }

    @Override
    public void positionProportionally(Float screenProportionX, Float screenProportionY) {
        setOriginCenter();
        /*
         *	Implementation note:
         *
         *  Though H5EGraphicElement supports proportional positioning, we do not use that mechanism
         *  as it introduces undesired side-effects on the rendering of sprites that assemble the
         *
         *  More specifically, due to round-off errors "seams" between the sprites become visible.
         *  This implementation makes sure to only use int precision, and simply overrides the existing
         *  coordinates.
         */

        if (screenProportionX != null && (screenProportionX < 0 || screenProportionX > 1)) {
            throw new IllegalArgumentException("Proportional position value must be a fraction between 0 and 1, or NULL");
        }
        if (screenProportionY != null && (screenProportionY < 0 || screenProportionY > 1)) {
            throw new IllegalArgumentException("Proportional position value must be a fraction between 0 and 1, or NULL");
        }

        int newX = screenProportionX == null ? 0 : (int)(screenProportionX * getEngine().getWidth());
        int newY = screenProportionY == null ? 0 : (int)(screenProportionY * getEngine().getHeight());

//        // Limit the top of the dialog box to the top of the screen
//        if (screenProportionY != null) {
//            float engineHeight = getEngine().getHeight();
//            newY = Math.min((int)(engineHeight - getHeight() / 2f), (int)(screenProportionY * engineHeight));
//        }

        // override the pre-existing coordinates with the new coordinates
        setX(newX, Align.center);
        setY(newY, Align.center);

        capTop();
    }

    /**
     * Limit the top of the dialog box to the top of the screen
     */
    public void capTop () {
        int newY = Math.min((int)(getEngine().getHeight() - getHeight() / 2f), (int)(getY() + getHeight() / 2f));
        setY(newY, Align.center);
    }

    /**
     * Limit the bottom of the dialog box to the bottom of the screen
     */
    public void capBottom () {
        int newY = Math.max((int)(getHeight() / 2f), (int)(getY() + getHeight() / 2f));
        setY(newY, Align.center);
    }

    /**
     * Limit the right side of the dialog box to the right side of the screen
     */
    public void capRight () {
        int newX = Math.min((int)(getEngine().getWidth() - getWidth() / 2f), (int)(getX() + getWidth() / 2f));
        setX(newX, Align.center);
    }

    /**
     * Limit the left side of the dialog box to the left side of the screen
     */
    public void capLeft () {
        int newX = Math.max((int)(getWidth() / 2f), (int)(getX() + getWidth() / 2f));
        setX(newX, Align.center);
    }

    /**
     * Tile windows next to each other in proportion to the screen
     * @param screenProportionX The proportion of the screen to center the windows
     * @param windows A list of windows to tile next to the current window
     */
    public void positionXProportionally (float screenProportionX, GCWindow... windows) {
        float totalWidth = this.getWidth();

        if (windows != null) {
            for (GCWindow window : windows) {
                if (window != null) {
                    totalWidth += window.getWidth();
                }
            }

            float centerX = screenProportionX * getEngine().getWidth();
            float leftX = centerX - totalWidth / 2f;
            float cumulativeWidth = this.getWidth();

            this.setX(UPMath.round(leftX + this.getWidth() / 2f), Align.center);

            for (GCWindow window : windows) {
                window.setX(UPMath.round(leftX + cumulativeWidth + window.getWidth() / 2f), Align.center);
                cumulativeWidth += window.getWidth();
            }
        } else {
            this.setX(UPMath.round(screenProportionX * getEngine().getWidth()), Align.center);
        }
    }

    /**
     * Tile windows on top of each other in proportion to the screen
     * @param screenProportionY The proportion of the screen to center the windows
     * @param windows A list of windows to tile on top of the current window
     */
    public void positionYProportionally (float screenProportionY, GCWindow... windows) {
        float totalHeight = this.getHeight();

        if (windows != null) {
            for (GCWindow window : windows) {
                if (window != null) {
                    totalHeight += window.getHeight();
                }
            }

            float centerY = screenProportionY * getEngine().getHeight();
            float leftY = centerY - totalHeight / 2f;
            float cumulativeHeight = this.getHeight();

            this.setY(UPMath.round(leftY + this.getHeight() / 2f), Align.center);

            for (GCWindow window : windows) {
                window.setY(UPMath.round(leftY + cumulativeHeight + window.getHeight() / 2f), Align.center);
                cumulativeHeight += window.getHeight();
            }
        } else {
            this.setY(UPMath.round(screenProportionY * getEngine().getHeight()), Align.center);
        }
    }
//
//    public void positionLike(GCWindow other) {
//        setOriginCenter();
//        setX(other.getX());
//        setY(other.getY());
//    }
//
//    public void positionBelow(GCWindow other) {
//        setOriginTopRight();
//        setRelativeToPositionOf(other.window, -other.getContentPaddingX(), 0);
//    }

    /**
     * Ub-binds this window element from the framework.
     * This is done when the window is no longer needed, to free up memory/resources.
     */
    @Override
    public void destroy() {
        if(getLayer() == null) {
            return;
        }
        getWindowManager().windowClosed(this);
        remove();
    }

    @Override
    public void open() {
        open(packOnOpen);
    }

    public void open(boolean pack) {
        if (isOpen()) {
            toFront();
            return;
        }
        if (pack) {
            pack();
        }
        setVisible(true);
        getWindowManager().activateWindow(this);

        onOpen.fire();
        toFront();
    }

    @Override
    public void close() {
        close(false);
    }

    /**
     * @param byUserInteraction pass TRUE to indicate that the window is being closed as a result of direct interaction by the user
     */
    public void close(boolean byUserInteraction) {
        if (!isOpen()) {
            return;
        }

        setVisible(false);
        getWindowManager().windowClosed(this);

        onClose();
    }

    protected void onClose() {
        onClose.fire();
    }

    @Override
    public boolean isOpen() {
        return isVisible();
    }

    /**
     * Makes this the active window, unless it is already active
     */
    @Override
    public void activate() {
        if (isOpen()) {
            getWindowManager().activateWindow(this);
            toFront();
        }
    }

    protected GCWindowManager getWindowManager() {
        return GCWindowManager.getInstance(getEngine());
    }

    @Override
    public int getContentPaddingX() {
        return CONTENT_PADDING_W;
    }

    @Override
    public int getContentPaddingY() {
        return CONTENT_PADDING_H;
    }

    @Override
    public int getTitleBarHeight() {
        return TITLE_BAR_HEIGHT;
    }


    @SuppressWarnings("unused")
    public static class GCWindowStyle extends WindowStyle {

        public GCWindowStyle() {
        }

        public GCWindowStyle(BitmapFont titleFont, Color titleFontColor, Drawable background) {
            super(titleFont, titleFontColor, background);
        }

        public GCWindowStyle(GCWindowStyle style) {
            super(style);
            this.titleBackground = style.titleBackground;
            this.closeButtonPadRight = style.closeButtonPadRight;
            this.closeButtonPadTop = style.closeButtonPadTop;
            this.labelPadLeft = style.labelPadLeft;
            this.labelPadBottom = style.labelPadBottom;
        }

        public Drawable titleBackground = null;
        public float closeButtonPadRight = -25;
        public float closeButtonPadTop = -5;
        public float labelPadLeft = 20;
        public float labelPadBottom = 6;

    }

    public boolean isPackOnOpen() {
        return packOnOpen;
    }

    public void setPackOnOpen(boolean packOnOpen) {
        this.packOnOpen = packOnOpen;
    }

    @Override
    public void pack() {
        // Here we want to remember what the hight of the window was before packing so we can make force the window
        // to grow  downwards instead of growing upwards (which is a problem because the title bar was getting pushed up)
        float beforeChangeHeight = getHeight();

        super.pack();

        // Now move the window down based on it's resize
        float delta = getHeight()-beforeChangeHeight;
        setY(getY()-delta);
    }
}
