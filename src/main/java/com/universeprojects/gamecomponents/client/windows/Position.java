package com.universeprojects.gamecomponents.client.windows;

public enum Position {

    /**
     * The element coordinates are relative to the window's content pane
     */
    WINDOW,

    /**
     * The element is placed within a new line
     */
    NEW_LINE,

    /**
     * The element is placed in the last line used (the "current" line).
     * A new line will be created only if there are no lines yet
     */
    SAME_LINE,

}
