package com.universeprojects.gamecomponents.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;

public class LoadingAnimation implements Disposable {

    private SpriteBatch spriteBatch;
    private float time = 0f;
    private Animation<TextureRegion> animation;
    private Texture loadingSheet;

    public LoadingAnimation() {
        loadingSheet = new Texture(Gdx.files.internal("images/GUI/ui2/preload/loading-sheet.png"));
        //loadingSheet = new Texture(Gdx.files.internal("images/loading-bar-background.png"));
        TextureRegion[][] regions = TextureRegion.split(loadingSheet, 64, 64);
        TextureRegion[] frames = new TextureRegion[18];
        int index = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 6; j++) {
                frames[index++] = regions[i][j];
            }
        }
        animation = new Animation<>(0.1f, frames);
        spriteBatch = new SpriteBatch();

    }

    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // Clear screen
        Gdx.gl.glClearColor(0.8f, 0.8f, 0.8f, 1f);
        time += Gdx.graphics.getDeltaTime(); // Accumulate elapsed animation time

        // Get current frame of animation for the current stateTime
        TextureRegion currentFrame = animation.getKeyFrame(time, true);
        spriteBatch.begin();
        spriteBatch.draw(currentFrame, 800 - 32, 450 - 32); // Draw current frame at (50, 50)
        spriteBatch.end();
    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
        loadingSheet.dispose();
    }
}
